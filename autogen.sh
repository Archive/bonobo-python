#!/bin/sh
# Run this to generate all the initial makefiles, etc.

DIE=0

echo `cat THIS_MODULE_IS_DEAD`
exit 255

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

ORIGDIR=`pwd`
cd $srcdir

test -z "$AUTOMAKE" && AUTOMAKE=automake
test -z "$ACLOCAL" && ACLOCAL=aclocal

(autoconf --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have autoconf installed to compile gnome-python."
	echo "Download the appropriate package for your distribution,"
	echo "or get the source tarball at ftp://ftp.gnu.org/pub/gnu/"
	DIE=1
}

($AUTOMAKE --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have automake installed to compile gnome-python."
	echo "Get ftp://sourceware.cygnus.com/pub/automake/automake1.4d.tar.gz"
	echo "(or a newer version if it is available)"
	DIE=1
}

grep -q 'PYTHON' `which $AUTOMAKE` > /dev/null 2>&1 || {
	echo
	echo "You must patch your copy of automake, since it doesn't"
	echo "contain the changes found in the am-changes directory"
	echo "OR:"
	echo "download and install automake-1.4d.tar.gz from"
	echo "ftp://sourceware.cygnus.com/pub/automake/."
	echo "But make sure to install it in the same prefix as your other auto* tools."
	echo "(and remove old version first)"
	DIE=1
}


if test "$DIE" -eq 1; then
	exit 1
fi

test -d bonobo || {
	echo "You must run this script in the top-level bonobo-python directory"
	exit 1
}

echo "$ACLOCAL -I macros $ACLOCAL_FLAGS"
$ACLOCAL -I macros $ACLOCAL_FLAGS
echo "autoheader"
autoheader
echo "$AUTOMAKE --add-missing --copy"
$AUTOMAKE --add-missing --copy
echo "autoconf"
autoconf

if test -z "$*"; then
	echo "I am going to run ./configure with no arguments - if you wish "
        echo "to pass any to it, please specify them on the $0 command line."
fi

./configure "$@"

echo 
echo "Now type 'make' to compile bonobo-python."
