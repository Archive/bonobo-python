#!/usr/bin/env python
# Simple moniker test.
# You must have evolution started to get this running
import bonobo
import gtk
import CORBA

# IDL
import Bonobo
from GNOME.Evolution import Composer

def test_moniker():
    composer = bonobo.get_object ('OAFIID:GNOME_Evolution_Mail_Composer',
                                  'GNOME/Evolution/Composer')
    print 'COMPOSER', composer
    recp = Composer.Recipient (name='Johan Dahlin',
                               address='zilch.am@home.se')
    composer.setHeaders ((recp,), (), (), 'Subject')
    composer.setBodyText ('This is really Fun!')
    composer.attachData ('text/plain', '/etc/fstab', 'Here is my fstab',
                         1, open ("/etc/fstab").read ())
    composer.show ()
    
gtk.idle_add (test_moniker)
bonobo.main ()
