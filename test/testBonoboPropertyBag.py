#!/usr/bin/python
import bonobo
import gtk

def foo (): pass

def test_control ():
    widget = gtk.GtkLabel ("h0hi")
    pb = bonobo.BonoboPropertyBag (foo, foo, widget)

    print "Instance :", pb
    print "Object   :", pb._o
    pb.set_value ("test-tool", "test")
    
gtk.idle_add (test_control)
bonobo.main ()
