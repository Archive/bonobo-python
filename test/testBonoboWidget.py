#!/usr/bin/env python
import bonobo
import oaf
import gtk

def test_widget ():
    win = bonobo.BonoboWindow ("my-container", 
                              "GtkHTML edtior in a PYTHON container!")
    
    uic = bonobo.BonoboUIContainer ()
    uic.set_engine (win.get_ui_engine ())
    
    box = gtk.GtkVBox ()
    win.set_contents (box)
    
    control = bonobo.BonoboWidget("OAFIID:GNOME_GtkHTML_Editor", uic.corba_objref())
    box.add (control)
    
    win.show_all ()
    
    return 0

gtk.idle_add (test_widget)
bonobo.main ()
