#!/usr/bin/env python
import bonobo
import gtk

def test_control():
    widget = gtk.GtkButton("TestButton")
    control = bonobo.BonoboControl(widget)
    component = bonobo.BonoboUIComponent("name")

    control.set_automerge(1)
#    control.set_properties(pb)
    control.set_ui_component(component)
#    control.set_property ("h0h0")
    
    print "Instance   :", control
    print "Object     :", control._o
    print "Widget     :", control.get_widget()
    print "Automerge  :", control.get_automerge()
#    print "Properties :", control.get_properties()
#    print "Property   :", control.get_property("h0h0")
    print "UIComponent:", control.get_ui_component()
#    print "UIContainer:", control.get_remote_ui_container()
    gtk.mainquit()

gtk.idle_add (test_control)
bonobo.main()
