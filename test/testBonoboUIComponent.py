#!/usr/bin/env python
import bonobo
import gnome.ui
import gdkpixbuf
import gtk

ui_data = """<Root>
  <commands>
    <cmd name="Toggle1" _label="_Toggle 1" _tip="Number one"/>
    <cmd name="Toggle2" _label="_Toggle 2" _typ="Number two"/>
    <cmd name="Radio1" _label="_Radio 1" _tip="Number one"/>
    <cmd name="Radio2" _label="_Radio 2" _typ="Number two"/>        
  </commands>   

  <menu>
    <submenu name="File" _label="_File">
      <menuitem name="FileExit" _label="E_xit" verb=""/>
    </submenu>
    <submenu name="Test" _label="_Test">
      <submenu _label="_Toggle">
	<menuitem id="Toggle1" type="toggle"/>
	<menuitem id="Toggle2" type="toggle"/>
      </submenu>
      <submenu _label="_Radio">
	<menuitem id="Radio1" type="radio" group="radio_group"/>
	<menuitem id="Radio2" type="radio" group="radio_group"/>
      </submenu>	      
    </submenu>
    <submenu name="Help" _label="_Help">
      <menuitem name="HelpAbout" _label="_About" verb=""/>
    </submenu>    
  </menu>
  
  <dockitem name="Toolbar" relief="none" behavior="exclusive"
              _tip="Main toolbar" hlook="text" vlook="icon"> 
     <toolitem name="New"
	       pixtype="stock" 
	       pixname="New"
	       verb="ToolbarNew"/>
     <toolitem name="New"
	       pixtype="stock" 
	       pixname="Save"
	       verb="ToolbarSave"/>	       
  </dockitem>
  
</Root>
"""


def verb_help_about (component, name):
    about = gnome.ui.GnomeAbout ("bonobo-python test", "0.0.0", 
                                 comments="A small test for bonobo-python that demonstrates verbs")
    about.show()

def verb_file_exit (component, name):
    gtk.mainquit ()
    
def verb_toolbar (component, name):
    dialog = gnome.ui.GnomeOkDialog ("%s clicked" % name)
    dialog.show()

verblist = { 
  "FileExit"    : verb_file_exit,
  "HelpAbout"   : verb_help_about,
  "ToolbarNew"  : verb_toolbar,
  "ToolbarSave" : verb_toolbar
}   

def ui_listener (*args):
    print "UIListen", args
    
# Window
window = bonobo.BonoboWindow ("Bonobo-python verb test")
window.connect ("delete_event", gtk.mainquit)
window.set_usize (320, 180)

# Label
label = gtk.GtkLabel ("Test label")
window.set_contents (label)

# Container, component & ui stuff
container = bonobo.BonoboUIContainer ()
component = bonobo.BonoboUIComponent ()
component.add_listener ("Toggle1", ui_listener, "foo")
component.add_listener ("Toggle2", ui_listener, "bar")
component.add_listener ("Radio1", ui_listener, "baz")
container.set_engine (window.get_ui_engine ())
component.set_container (container.corba_objref ())
component.add_verb_list (verblist)
component.set_translate ("/", ui_data)

window.show_all ()

bonobo.main ()


