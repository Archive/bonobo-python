#!/usr/bin/env python
import sys
import bonobo
import CORBA

def listener1 (listener, name, value, extra):
    print "Listener 1:", name, "=", value.value, extra

def listener2 (listener, name, value):
    print "Listener 2:", name, "=", value.value
    
es = bonobo.BonoboEventSource()
corba_es = es.corba_objref()

bonobo.event_source_client_add_listener (corba_es, listener1, "name", 1)
bonobo.event_source_client_add_listener (corba_es, listener2, "version")
#bonobo.event_source_client_add_listener (corba_es, listener3, "name,version")
any_string = CORBA.Any (CORBA.TypeCode ("IDL:CORBA/String:1.0"), "bonobo-python")
any_long   = CORBA.Any (CORBA.TypeCode ("IDL:CORBA/Float:1.0"), 0.1)

es.notify_listeners ("name", any_string)
es.notify_listeners ("version", any_long)
