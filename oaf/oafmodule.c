
/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* bonobo-python - Bonobo bindings for python.
 * Copyright (C) 2001 Johan Dahlin <zilch.am@home.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Johan Dahlin
 *
 * oafmodule.c : OAF bindnings.
 *
 */

#include <Python.h>
#include <sysmodule.h>
#include <orbit-python/orbit-python.h>
#include <liboaf/liboaf.h>
#include <liboaf/oaf-async.h>

static CORBA_ORB_PyObject *orb = NULL;
static PyObject           *dict;

/* stolen from oaf-private.h */
extern CORBA_Object oaf_activation_context_get (void);

extern CORBA_Object oaf_object_directory_get   (const char *username,
                                                const char *hostname,
                                                const char *domain);

/* CORBA_ORB oaf_init (int argc, char **argv);
 *
 */
static PyObject *
wrap_oaf_init (PyObject *self,
	       PyObject *args)
{
	PyObject  *av;
	int        argc;
	int        i;
	char     **argv;

	if (oaf_is_initialized ()) {
		Py_INCREF (Py_None);
		return Py_None;
	}

	if (!PyArg_ParseTuple (args, ":oaf_init")) {
		return NULL;
	}

	av = PySys_GetObject ("argv");
	argc = PyList_Size (av);
	
	argv = malloc (argc * sizeof (char*));
	for (i = 0; i < argc; i++) {
		argv[i] = strdup (PyString_AsString (PyList_GetItem (av, i)));
	}
    
	orb = PyORBit_ORB_New (oaf_init (argc, argv));
	if (orb == NULL) {
		Py_INCREF (Py_None);
		return Py_None;
	}

	PyDict_SetItemString (dict, "is_initialized", PyInt_FromLong (1L));

	Py_INCREF ((PyObject*) orb);
	return (PyObject*) orb;
}

/* gboolean oaf_is_initialized (void);
 *
 */
static PyObject *
wrap_oaf_is_initialized (PyObject *self,
			 PyObject *args)
{
	if (!PyArg_ParseTuple (args, ":oaf_is_initialized")) {
		return NULL;
	}
	
	return PyInt_FromLong (oaf_is_initialized ());
}

/* CORBA_ORB oaf_orb_get (void);
 *
 */
static PyObject *
wrap_oaf_orb_get (PyObject *self,
		  PyObject *args)
{
	if (!oaf_is_initialized ()) {
		Py_INCREF (Py_None);
		return Py_None;
	}
		
	if (!PyArg_ParseTuple (args, ":oaf_orb_get")) {
		return NULL;
	}

	if (orb != NULL) {
		Py_INCREF (orb);
		return (PyObject*)orb;
	}

	orb = PyORBit_ORB_New (oaf_orb_get ());
	if (orb == NULL) {
		Py_INCREF (Py_None);
		return Py_None;
	}

	PyDict_SetItemString (dict, "is_initialized", PyInt_FromLong (1L));

	Py_INCREF ((PyObject*) orb);
	return (PyObject*) orb;
}

/*
 *
 */
static PyObject *
wrap_oaf_get_popt_table_name (PyObject *self,
			      PyObject *args)
{
	char *name;
    
	if (!PyArg_ParseTuple (args, ":oaf_get_popt_table_name")) {
		return NULL;
	}
	name = oaf_get_popt_table_name ();

	if (name == NULL) {
		return NULL;
	}
    
	return Py_BuildValue ("s", name);
    
}

/* OAF_ServerInfoList* oaf_query (const char *requirements,
 *                                char *const *selection_order,
 *                                CORBA_Environment *ev);
 */
static PyObject *
wrap_oaf_query (PyObject *self,
		PyObject *args)
{
	PyObject            *pysort;
	guint                i;
	gchar               *query;
	guint                len;
	gchar              **sort;
	CORBA_Environment    ev;
	OAF_ServerInfoList  *infolist;
	PyObject            *pyinfo;
	OAF_ServerInfo       server;
	PyObject            *infodict;
	PyObject            *propdict;
	guint                j;
	PyObject            *value;
	guint                k;
	GNOME_stringlist     strlist;
	GString*             str;
	gchar               *ret;
	
	pysort = NULL;
	i = 0;

	if (!PyArg_ParseTuple (args, "s|O:oaf_query", &query, &pysort)) {
		return NULL;
	}

	/* Create sortlist */
	if (pysort != NULL) {
		if (!PyList_Check (pysort)) {
			return NULL;
		}
		
		len = PyList_Size (pysort);
		sort = malloc (len * sizeof (char*));
		for (i = 0; i < len; i++)
			sort[i] = strdup (PyString_AsString (PyList_GetItem (pysort, i)));
		sort[i] = NULL;
	} else {
		sort = NULL;
	}

	CORBA_exception_init (&ev);

	infolist = oaf_query (query, sort, &ev);
	
	if (infolist == NULL) {
		CORBA_exception_free (&ev);
		Py_INCREF (Py_None);
		return Py_None;
	}

    /* Create a list of dictionaries */
	pyinfo = PyList_New (infolist->_length);
	for (i = 0; i < infolist->_length; i++) {
		server = infolist->_buffer[i];
		
		infodict = PyDict_New();
		
		PyDict_SetItemString (infodict, "iid", PyString_FromString (server.iid));
		
		PyDict_SetItemString (infodict, "server_type",
				      PyString_FromString (server.server_type));
		
		PyDict_SetItemString (infodict, "location_info",
				      PyString_FromString (server.location_info));
		
		PyDict_SetItemString (infodict, "username",
				      PyString_FromString (server.username));
		
		PyDict_SetItemString (infodict, "hostname",
				      PyString_FromString (server.hostname));
		
		PyDict_SetItemString (infodict, "domain",
				      PyString_FromString (server.domain));
		
		propdict = PyDict_New ();
		for (j = 0; j < server.props._length; j++) {
			value = Py_None;
			
			switch (server.props._buffer[j].v._d) {
			case OAF_P_STRING:
				value = Py_BuildValue ("s", server.props._buffer[j].v._u.value_string);
				break;
			case OAF_P_NUMBER:
				value = Py_BuildValue ("i", server.props._buffer[j].v._u.value_number);
				break;
			case OAF_P_BOOLEAN:
				value = Py_BuildValue ("b", server.props._buffer[j].v._u.value_boolean);
				break;
			case OAF_P_STRINGV:
#if PY_MAJOR_VERSION >= 2
				strlist = server.props._buffer[j].v._u.value_stringv;
				for (k = 0, str = NULL; k < strlist._length; k++) {
					if (str == NULL) {
						str = g_string_new (strlist._buffer[k]);
					} else {
						str = g_string_append (str, strlist._buffer[k]);
					}
				}
				if (str) {
					ret = g_strdup (str->str);
					g_string_free (str, TRUE);
					value = Py_BuildValue ("u", ret);
				}
#endif                    
				break;
			}
			
			if (value != Py_None) {
				PyDict_SetItemString (propdict,
						      server.props._buffer[j].name,
						      value);
				Py_DECREF (value);
			}
		}
		
		PyDict_SetItemString (infodict, "props", propdict);
		Py_DECREF (propdict);
		PyList_SetItem (pyinfo, i, infodict);
	}
	
	if (infolist) {
		CORBA_free (infolist);
	}

	CORBA_exception_free (&ev);
	
	Py_INCREF (pyinfo);
	return pyinfo;
}

/* CORBA_Object oaf_activate (const char *requirements,
 *                            char *const *selection_order,
 *                            OAF_ActivationFlags flags,
 *                            OAF_ActivationID *ret_aid,
 *                            CORBA_Environment *ev);
 */
static PyObject *
wrap_oaf_activate (PyObject *self,
		   PyObject *args)
{
	gchar              *requirements;
	PyObject           *pysort;
	glong               flags;
	gchar              *ret_aid;
	guint               len;
	char              **sort;
	guint               i;
	CORBA_Environment   ev;
	CORBA_Object        corba_object;


	flags = 0L;
	ret_aid = NULL;
	if (!PyArg_ParseTuple (args, "zO|ls:oaf_activate",
			       &requirements,
			       &pysort,
			       &flags,
			       &ret_aid)) {
		return NULL;
	}
	
	if (pysort != NULL) {
		len = PyList_Size (pysort);
		sort = malloc (len+1 * sizeof(char*));
		for (i = 0; i < len; i++) {
			sort[i] = strdup (PyString_AsString (PyList_GetItem (pysort, i)));
		}
		sort[i] = NULL;
	} else {
		sort = NULL;
	}
	
	CORBA_exception_init (&ev);
	corba_object = oaf_activate (requirements, sort, flags,
				     (OAF_ActivationID *)ret_aid, &ev);
	
	if (!corba_object) {
		CORBA_exception_free (&ev);
		Py_INCREF (Py_None);
		return Py_None;
	}
	
	CORBA_exception_free (&ev);
	
	return (PyObject*)PyORBit_Object_New (corba_object);
}

/* CORBA_Object oaf_activate_from_id (const OAF_ActivationID aid,
 *                                    OAF_ActivationFlags flags,
 *                                    OAF_ActivationID *ret_aid,
 *                                    CORBA_Environment *ev);
 *
 */
static PyObject *
wrap_oaf_activate_from_id (PyObject *self,
			   PyObject *args)
{
	CORBA_Object       object = NULL;
	CORBA_Environment  ev;
	char              *activation_id;
	OAF_ActivationID  *ret_aid = NULL;
	long               flags = 0;
	
	if (PyArg_ParseTuple (args, "s|ls:activate_from_id",
			      &activation_id, &flags, &ret_aid) == 0) {
		return NULL;
	}

	CORBA_exception_init (&ev);
	object = oaf_activate_from_id (activation_id, flags,
				       ret_aid, &ev);
	
	if (ev._major != CORBA_NO_EXCEPTION || object == NULL) {
		Py_FatalError ("Could not activate object.\n");
	}
	
	CORBA_exception_free (&ev);
	
	return (PyObject*)PyORBit_Object_New (object);
}

/* void oaf_activate_async (const char *requirements, char *const *selection_order,
 *                          OAF_ActivationFlags flags,
 *                          OAFActivationCallback callback,
 *                          gpointer user_data, CORBA_Environment *ev)
 */
static PyObject *
wrap_oaf_activate_async (PyObject *self,
			 PyObject *args)
{
	CORBA_Environment ev;
	PyObject *sel_order;
	PyObject *func, *extra = NULL, *data;
	char *req;
	char **sort;
	long flags;
	int len, i;
	
	if (!PyArg_ParseTuple (args, "sOiO|O!:oaf_activate_async",
			       &req, &sel_order, &flags, &func, &extra)) {
		return NULL;
	}
		
	if (!PyCallable_Check(func)) {
		PyErr_SetString(PyExc_TypeError, "fourth argument must be callable");
		return NULL;
	}
		
	if (extra) {
		Py_INCREF (extra);
	} else {
		extra = PyTuple_New (0);
	}
	
	if (extra == NULL) {
		return NULL;
	}
	
	data = Py_BuildValue ("(ON)", func, extra);
	
	len = PyList_Size (sel_order);
	sort = malloc (len * sizeof(char*));
	for (i = 0; i < len; i++) {
		sort[i] = strdup (PyString_AsString (PyList_GetItem (sel_order, i)));
	}
	sort[i] = NULL;
		
	CORBA_exception_init (&ev);
	oaf_activate_async (req, sort, flags, (OAFActivationCallback)func, data, &ev);
	CORBA_exception_free (&ev);
	
	Py_INCREF (Py_None);
	return Py_None;
}

/* void oaf_activate_from_id_async (const OAF_ActivationID aid,
 *                                  OAF_ActivationFlags flags,
 *                                  OAFActivationCallback callback,
 *                                  gpointer user_data,
 *                                  CORBA_Environment *ev);
 */
static PyObject *
wrap_oaf_activate_from_id_async (PyObject *self,
				 PyObject *args)
{
	CORBA_Environment ev;
	PyObject          *func, *extra = NULL, *data;
	char              *activation_id;
	char              *ret_aid = NULL;
	long              flags = 0;
	
	if (PyArg_ParseTuple (args, "slsO|O!:activate_from_id_async",
			      &activation_id, &flags,
			      &ret_aid, &func, &extra) == 0) {
		return NULL;
	}
	
	if (!PyCallable_Check (func)) {
		PyErr_SetString (PyExc_TypeError, "fourth argument must be callable");
		return NULL;
	}
	
	if (extra) {
		Py_INCREF (extra);
	} else {
		extra = PyTuple_New (0);
	}
	
	if (extra == NULL) {
		return NULL;
	}
	
	data = Py_BuildValue ("(ON)", func, extra);
	
	CORBA_exception_init (&ev);
	
	oaf_activate_from_id_async (activation_id, flags,
				    (OAFActivationCallback)func, data, &ev);
	
	if (ev._major != CORBA_NO_EXCEPTION) {
		Py_FatalError ("Could not activate object(async).\n");
	}
	
	CORBA_exception_free (&ev);
	
	Py_INCREF (Py_None);
	return Py_None;
}

/* OAF_RegistrationResult oaf_active_server_register (const char *iid,
 *                                                    CORBA_Object obj);
 *
 */
static PyObject *
wrap_oaf_active_server_register (PyObject *self,
				 PyObject *args)
{
	CORBA_Environment       ev;
	PyObject               *object;
	OAF_RegistrationResult  res;
	char                   *activation_id;
	
	if (PyArg_ParseTuple (args, "sO:oaf_active_server_register",
			      &activation_id, &object) == 0) {
		return NULL;
	}
	
	CORBA_exception_init (&ev);
	res = oaf_active_server_register (activation_id, (CORBA_Object)PyORBit_Object_Get (object));
	CORBA_exception_free (&ev);
	
	if (res == OAF_REG_SUCCESS) {
		return Py_BuildValue ("s", "OAF_REG_SUCCESS");
	} else if (res == OAF_REG_NOT_LISTED) {
		return Py_BuildValue ("s", "OAF_REG_NOT_LISTED");
	} else if (res == OAF_REG_ALREADY_ACTIVE) {
		return Py_BuildValue ("s", "OAF_REG_ALREADY_ACTIVE");
	} else if (res == OAF_REG_ERROR) {
		return Py_BuildValue ("s", "OAF_REG_ERRORY");
	}
	
	Py_INCREF (Py_None);
	return Py_None;
}

/* OAF_RegistrationResult oaf_active_server_unregister (const char *iid,
 *                                                      CORBA_Object obj);
 *
 */
static PyObject *
wrap_oaf_active_server_unregister (PyObject *self,
				   PyObject *args)
{
	CORBA_Environment       ev;
	CORBA_PyObject         *object;
	char                   *activation_id;
	
	if (PyArg_ParseTuple (args, "sO:oaf_active_server_unregister",
			      &activation_id, &object) == 0) {
		return NULL;
	}
	
	CORBA_exception_init (&ev);
	oaf_active_server_unregister (activation_id, object->obj);
	
	if (ev._major != CORBA_NO_EXCEPTION || object == NULL) {
		Py_FatalError ("Could not unregister server.\n");
	}
	
	CORBA_exception_free (&ev);
	
	Py_INCREF (Py_None);
	return Py_None;
}

/*
 * 
 */
static PyObject *
wrap_oaf_name_service_get (PyObject *self,
			   PyObject *args)
{
	CORBA_Object      retval;
	CORBA_Environment ev;
   
	if (!PyArg_ParseTuple (args, ":oaf_name_service_get")) {
		return NULL;
	}
	
	CORBA_exception_init (&ev);
	retval = CORBA_Object_duplicate (oaf_name_service_get (&ev), &ev);
	CORBA_exception_free (&ev);
	
	return (PyObject*)PyORBit_Object_New (retval);						   
}

/*
 * 
 */
static PyObject *
wrap_oaf_activation_context_get (PyObject *self,
				 PyObject *args)
{
	CORBA_Object retval;
	CORBA_Environment ev;
   
	if (!PyArg_ParseTuple (args, ":oaf_activation_context_get")) {
		return NULL;
	}
		
	CORBA_exception_init (&ev);      
	retval = CORBA_Object_duplicate (oaf_activation_context_get (), &ev);
	CORBA_exception_free (&ev);
	
	return (PyObject*)PyORBit_Object_New (retval);						   
}

/*
 * 
 */
/* FIXME: When next oaf is released */
#if 0
static PyObject *
wrap_oaf_object_directory_get (PyObject *self,
			       PyObject* args)
{
	gchar             *username;
	gchar             *hostname;
	gchar             *domain;
	CORBA_Object       retval;
	CORBA_Environment  ev;

	username = NULL;
	hostname = NULL;
	domain = NULL;
	if (!PyArg_ParseTuple (args, "|sss:oaf_object_directory_get",
			       &username, &hostname, &domain)) {
		return NULL;
	}
	
	CORBA_exception_init (&ev);      
	retval = CORBA_Object_duplicate (oaf_object_directory_get (username, hostname, domain), &ev);
	CORBA_exception_free (&ev);
	
	return (PyObject*)PyORBit_Object_New (retval);
}
#endif 

/* const char    *oaf_hostname_get     (void)
 * 
 */
static PyObject *
wrap_oaf_hostname_get (PyObject *self,
		       PyObject *args)
{
	if (!PyArg_ParseTuple (args, ":oaf_hostname_get")) {
		return NULL;
	}
	
	return Py_BuildValue ("s", oaf_hostname_get ());
}

/* const char    *oaf_session_name_get (void)
 * 
 */
static PyObject *
wrap_oaf_session_name_get (PyObject *self,
			   PyObject *args)
{
	if (!PyArg_ParseTuple (args, ":oaf_session_name_get")) {
		return NULL;
	}
	
	return Py_BuildValue ("s", oaf_session_name_get ());
}

/* const char    *oaf_domain_get       (void)
 * 
 */
static PyObject *
wrap_oaf_domain_get (PyObject *self,
		     PyObject *args)
{
	if (!PyArg_ParseTuple (args, ":oaf_domain_get")) {
		return NULL;
	}
	
	return Py_BuildValue ("s", oaf_domain_get ());
}

static struct PyMethodDef oafmod_methods[] = {
	{ "init",                     wrap_oaf_init,                     METH_VARARGS },
	{ "is_initialized",           wrap_oaf_is_initialized,           METH_VARARGS },
	{ "orb_get",                  wrap_oaf_orb_get,                  METH_VARARGS },
	{ "get_popt_table_name",      wrap_oaf_get_popt_table_name,      METH_VARARGS },
	{ "query",                    wrap_oaf_query,                    METH_VARARGS },
	{ "activate",                 wrap_oaf_activate,                 METH_VARARGS },
	{ "activate_from_id",         wrap_oaf_activate_from_id,         METH_VARARGS },
/* { "activate_async",           wrap_oaf_activate_async,           METH_VARARGS }, */
/* { "activate_from_id_async",   wrap_oaf_activate_from_id_async,   METH_VARARGS }, */
	{ "active_server_register",   wrap_oaf_active_server_register,   METH_VARARGS },
	{ "active_server_unregister", wrap_oaf_active_server_unregister, METH_VARARGS },
/*{ "oaf_plugin_use",           wrap_oaf_plugin_use,               METH_VARARGS }, */
/*{ "oaf_plugin_unuse",         wrap_oaf_plugin_unuse,             METH_VARARGS }, */
	{ "name_service_get",         wrap_oaf_name_service_get,         METH_VARARGS },
	{ "activation_context_get",   wrap_oaf_activation_context_get,   METH_VARARGS },
/* FIXME: Wait for next version of OAF */
/*	{ "object_directory_get",     wrap_oaf_object_directory_get,     METH_VARARGS }, */
	{ "hostname_get",             wrap_oaf_hostname_get,             METH_VARARGS },
	{ "session_name_get",         wrap_oaf_session_name_get,         METH_VARARGS },
	{ "domain_get",               wrap_oaf_domain_get,               METH_VARARGS },
	
	{ NULL, NULL }
};

void
initoaf (void)
{
	PyObject *oafmodule;
	
	init_orbit_python ();
   
	oafmodule = Py_InitModule ("oaf", oafmod_methods);
	dict = PyModule_GetDict (oafmodule);
			      
	if (PyErr_Occurred ()) {
		Py_FatalError ("Can't initialise module oaf\n");
	}
}
