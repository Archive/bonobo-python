/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* bonobo-python - Bonobo bindings for python.
 * Copyright (C) 2001 Johan Dahlin <zilch.am@home.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Johan Dahlin
 *
 * nautilusmodule.c : Wrapper for libnautilus
 */

#include <Python.h>
#include <pygtk/pygtk.h>
#include <libnautilus/libnautilus.h>
#include <gconf/gconf.h>

GList *
PySequence_ToGList (PyObject *object)
{
	GList *list;
	int    i;
	int    size;
	gchar *tmp;
	
	if (!PySequence_Check (object)) {
		return NULL;
    }
	
	list = NULL;
	size = PySequence_Length (object);
	for (i = 0; i < size; i++) {
		tmp = g_strdup (PyString_AsString (PySequence_GetItem (object, i)));
		list = g_list_append (list, tmp);
	}
    
	return list;
}

/* GtkType nautilus_view_get_type (void);
 *
 */
static PyObject *
_wrap_nautilus_view_get_type (PyObject *self,
			      PyObject *args)
{                                                                  
	GtkType type;
                                                  
	if (!PyArg_ParseTuple (args, ":nautilus_view_get_type")) {
		return NULL;
	}
                                                                   
	type = nautilus_view_get_type ();
	
	return PyInt_FromLong (type);                                                       
}                                                                  

/* NautilusView * nautilus_view_new (GtkWidget *widget);
 *
 */
static PyObject *
_wrap_nautilus_view_new (PyObject *self,
			 PyObject *args)
{
	PyGtk_Object *widget;
    
	if (!PyArg_ParseTuple (args, "O!:nautilus_view_new",
			       &PyGtk_Type, &widget)) {
		return NULL;
	}

	return PyGtk_New (GTK_OBJECT (nautilus_view_new (
		GTK_WIDGET (PyGtk_Get (widget)))));
}

/* NautilusView *     nautilus_view_new_from_bonobo_control              (BonoboControl          *bonobo_control);
 *
 */
static PyObject *
_wrap_nautilus_view_new_from_bonobo_control (PyObject *self,
					     PyObject *args)
{
    PyGtk_Object *bonobo_control;
    
    if (!PyArg_ParseTuple (args, "O!:nautilus_view_new_from_bonobo_control",
                           &PyGtk_Type, &bonobo_control)) {
	    return NULL;
    }

    return PyGtk_New (
	    GTK_OBJECT (nautilus_view_new_from_bonobo_control (
		    BONOBO_CONTROL (PyGtk_Get (bonobo_control)))));
}

/* BonoboControl *    nautilus_view_get_bonobo_control                   (NautilusView           *view);
 *
 */
static PyObject *
_wrap_nautilus_view_get_bonobo_control (PyObject *self,
					PyObject *args)
{
    PyGtk_Object *view;
    
    if (!PyArg_ParseTuple (args, "O!:nautilus_view_get_bonobo_control",
                           &PyGtk_Type, &view)) {
	    return NULL;
    }

    return PyGtk_New (GTK_OBJECT (nautilus_view_get_bonobo_control (
	    NAUTILUS_VIEW (PyGtk_Get (view)))));
}

/* void               nautilus_view_open_location_in_this_window         (NautilusView           *view,
 *                                                                        const char             *location_uri);
 */
static PyObject *
_wrap_nautilus_view_open_location_in_this_window (PyObject *self,
						  PyObject *args)
{
	PyGtk_Object *view;
	char         *location_uri;
    
	if (!PyArg_ParseTuple (args, "Os!:nautilus_view_open_location_in_this_window",
			       &PyGtk_Type, &view, &location_uri)) {
		return NULL;
	}
    
	nautilus_view_open_location_in_this_window (
		NAUTILUS_VIEW (PyGtk_Get (view)), location_uri);
	
	Py_INCREF (Py_None);
	return Py_None;
}

/* void nautilus_view_open_location_prefer_existing_window (NautilusView *view,
 *                                                          const char   *location_uri);
 */                    
static PyObject *
_wrap_nautilus_view_open_location_prefer_existing_window (PyObject *self,
							  PyObject *args)
{
	PyGtk_Object *view;
	char         *location_uri;
    
	if (!PyArg_ParseTuple (args, "Os!:nautilus_view_open_location_prefer_existing_window",
			       &PyGtk_Type, &view, &location_uri)) {
		return NULL;
	}
    
	nautilus_view_open_location_prefer_existing_window (
		NAUTILUS_VIEW (PyGtk_Get (view)), location_uri);

	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_open_location_force_new_window       (NautilusView           *view,
 *                                                                        const char             *location_uri,
 *                                                                        GList                  *selection);
 */
static PyObject *
_wrap_nautilus_view_open_location_force_new_window (PyObject *self,
						    PyObject *args)
{
    PyGtk_Object *view;
    char         *location_uri;
    PyObject     *selection;

    
    if (!PyArg_ParseTuple (args, "O!sO:nautilus_view_open_location_force_new_window",
                           &PyGtk_Type, &view, &location_uri, &selection)) {
	    return NULL;
    }

    nautilus_view_open_location_force_new_window (NAUTILUS_VIEW (PyGtk_Get (view)),
                                                  location_uri,
                                                  PySequence_ToGList (selection));

    Py_INCREF (Py_None);
    return Py_None;
}

/* void               nautilus_view_report_location_change               (NautilusView           *view,
 *                                                                        const char             *location_uri,
 *                                                                        GList                  *selection, 
 *                                                                        const char             *title);
 */
static PyObject *
_wrap_nautilus_view_report_location_change (PyObject *self,
					    PyObject *args)
{
	PyGtk_Object *view;
	char         *location_uri;
	PyObject     *selection;
	char         *title;
    
	if (!PyArg_ParseTuple (args, "O!sOs:nautilus_view_report_location_change",
			       &PyGtk_Type, &view, &location_uri, &selection, &title)) {
		return NULL;
	}
	
	nautilus_view_report_location_change (NAUTILUS_VIEW (PyGtk_Get (view)),
					      location_uri,
					      PySequence_ToGList (selection),
					      title);
	
	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_report_redirect                      (NautilusView           *view,
 *                                                                        const char             *from_location_uri,
 *                                                                        const char             *to_location_uri,
 *                                                                        GList                  *selection, 
 *                                                                        const char             *title);
 */
static PyObject *
_wrap_nautilus_view_report_redirect (PyObject *self,
				     PyObject *args)
{
	PyGtk_Object *view;
	char         *from_location_uri;
	char         *to_location_uri;    
	PyObject     *selection;
	char         *title;
    
	if (!PyArg_ParseTuple (args, "O!ssOs:nautilus_view_report_redirect",
			       &PyGtk_Type, &view, &from_location_uri,
			       &to_location_uri, &selection, &title)) {
		return NULL;
	}
	
	nautilus_view_report_redirect (NAUTILUS_VIEW (PyGtk_Get (view)),
				       to_location_uri,
				       from_location_uri,
				       PySequence_ToGList (selection),
				       title);

	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_report_selection_change              (NautilusView           *view,
 *                                                                        GList                  *selection);
 */
static PyObject *
_wrap_nautilus_view_report_selection_change (PyObject *self,
					     PyObject *args)
{
	PyGtk_Object *view;
	PyObject     *selection;
    
	if (!PyArg_ParseTuple (args, "O!s:nautilus_view_report_selection_change",
			       &PyGtk_Type, &view, &selection)) {
		return NULL;
	}

	nautilus_view_report_selection_change (NAUTILUS_VIEW (PyGtk_Get (view)),
                                           PySequence_ToGList (selection));
	
	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_report_status                        (NautilusView           *view,
 *                                                                        const char             *status);
 */
static PyObject *
_wrap_nautilus_view_report_status (PyObject *self,
				   PyObject *args)
{
	PyGtk_Object *view;
	char         *status;
    
	if (!PyArg_ParseTuple (args, "Os!:nautilus_view_report_status",
			       &PyGtk_Type, &view, &status)) {
		return NULL;
	}
	
	nautilus_view_report_status (NAUTILUS_VIEW (PyGtk_Get (view)), status);

	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_report_load_underway                 (NautilusView           *view);
 *
 */
static PyObject *
_wrap_nautilus_view_report_load_underway (PyObject *self,
					  PyObject *args)
{
	PyGtk_Object *view;
    
	if (!PyArg_ParseTuple (args, "O!:nautilus_view_report_load_underway",
			       &PyGtk_Type, &view)) {
		return NULL;
	}
    
	nautilus_view_report_load_underway (NAUTILUS_VIEW (PyGtk_Get (view)));

	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_report_load_progress                 (NautilusView           *view,
 *                                                                        double                  fraction_done);
 */
static PyObject *
_wrap_nautilus_view_report_load_progress (PyObject *self,
					  PyObject *args)   
{
	PyGtk_Object *view;
	double        fraction_done;
        
	if (!PyArg_ParseTuple (args, "O!d:nautilus_view_report_load_progress",
			       &PyGtk_Type, &view, &fraction_done)) {
		return NULL;
	}
	
	nautilus_view_report_load_progress (NAUTILUS_VIEW (PyGtk_Get (view)), fraction_done);

	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_report_load_complete                 (NautilusView           *view);
 *
 */
static PyObject *
_wrap_nautilus_view_report_load_complete (PyObject *self,
					  PyObject *args)   
{
	PyGtk_Object *view;
    
	if (!PyArg_ParseTuple (args, "O!:nautilus_view_report_load_complete",
			       &PyGtk_Type, &view)) {
		return NULL;
	}
	
	nautilus_view_report_load_complete (NAUTILUS_VIEW (PyGtk_Get (view)));

	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_report_load_failed                   (NautilusView           *view);
 *
 */
static PyObject *
_wrap_nautilus_view_report_load_failed (PyObject *self,
					PyObject *args)   
{
	PyGtk_Object *view;
    
	if (!PyArg_ParseTuple (args, "O!:nautilus_view_report_load_failed",
			       &PyGtk_Type, &view)) {
		return NULL;
	}
	
	nautilus_view_report_load_failed (NAUTILUS_VIEW (PyGtk_Get (view)));

	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_set_title                            (NautilusView           *view,
 *                                                                        const char             *title);
 */
static PyObject *
_wrap_nautilus_view_set_title (PyObject *self,
			       PyObject *args)   
{
	PyGtk_Object  *view;
	char          *title;
    
	if (!PyArg_ParseTuple (args, "Os!:nautilus_view_set_title",
			       &PyGtk_Type, &view, &title)) {
		return NULL;
	}
    
	nautilus_view_set_title (NAUTILUS_VIEW (PyGtk_Get (view)), title);

	Py_INCREF (Py_None);
	return Py_None;
}

/* void               nautilus_view_go_back                              (NautilusView           *view);
 *
 */
static PyObject *
_wrap_nautilus_view_go_back (PyObject *self,
			     PyObject *args)
{
	PyGtk_Object *view;
    
	if (!PyArg_ParseTuple (args, "O!:nautilus_view_go_back",
			       &PyGtk_Type, &view)) {
		return NULL;
	}
    
	nautilus_view_go_back (NAUTILUS_VIEW (PyGtk_Get (view)));

	Py_INCREF (Py_None);
	return Py_None;
}

static struct PyMethodDef nautilusmodule_methods[] =
{
    { "nautilus_view_get_type",                             _wrap_nautilus_view_get_type, METH_VARARGS },
    { "nautilus_view_new",                                  _wrap_nautilus_view_new, METH_VARARGS },
    { "nautilus_view_new_from_bonobo_control",              _wrap_nautilus_view_new_from_bonobo_control, METH_VARARGS },
    { "nautilus_view_get_bonobo_control",                   _wrap_nautilus_view_get_bonobo_control, METH_VARARGS },
    { "nautilus_view_open_location_in_this_window",         _wrap_nautilus_view_open_location_in_this_window, METH_VARARGS },
    { "nautilus_view_open_location_prefer_existing_window", _wrap_nautilus_view_open_location_prefer_existing_window, METH_VARARGS },
    { "nautilus_view_open_location_force_new_window",       _wrap_nautilus_view_open_location_force_new_window, METH_VARARGS },
    { "nautilus_view_report_location_change",               _wrap_nautilus_view_report_location_change, METH_VARARGS },
    { "nautilus_view_report_redirect",                      _wrap_nautilus_view_report_redirect, METH_VARARGS },
    { "nautilus_view_report_selection_change",              _wrap_nautilus_view_report_selection_change, METH_VARARGS },
    { "nautilus_view_report_status",                        _wrap_nautilus_view_report_status, METH_VARARGS },
    { "nautilus_view_report_load_underway",                 _wrap_nautilus_view_report_load_underway, METH_VARARGS },
    { "nautilus_view_report_load_progress",                 _wrap_nautilus_view_report_load_progress, METH_VARARGS },
    { "nautilus_view_report_load_complete",                 _wrap_nautilus_view_report_load_complete, METH_VARARGS },
    { "nautilus_view_report_load_failed",                   _wrap_nautilus_view_report_load_failed, METH_VARARGS },
    { "nautilus_view_set_title",                            _wrap_nautilus_view_set_title, METH_VARARGS },        
    { "nautilus_view_go_back",                              _wrap_nautilus_view_go_back, METH_VARARGS },
};

void
init_nautilus (void)
{
	PyObject *nautilus_module;

	/* FIXME: Init bonobo-python */
    
	nautilus_module = Py_InitModule ("_nautilus", nautilusmodule_methods);
	
	if (PyErr_Occurred ()) {
		Py_FatalError ("Can't initialise module _bonobo\n");
	}
}
