import _nautilus

import bonobo
import gtk

class NautilusView (bonobo.BonoboObject):
    get_type = _nautilus.nautilus_view_get_type
    def __init__ (self, object=None, _obj=None):
        if _obj: self._o = _obj; return

        if isinstance (obj, gtk.GtkWidget):
            self._o = _nautilus.nautilus_view_new (object._o)
        elif isinstance (obj, bonobo.BonoboControl):
            self._o = _nautilus.nautilus_view_new_from_bonobo_control (object._o)

    def get_bonobo_control (self):
        return bonobo.BonoboControl (_obj=_nautilus.nautilus_get_bonobo_control (self._o))
        
    def open_location_in_this_window (self, location_uri):
        _nautilus.nautilus_view_open_location_in_this_window (self._o, location_uri)

    def open_location_prefer_existing_window (self, location_uri):
        _nautilus.nautilus_view_open_location_prefer_existing_window (self._o, location_uri)
        
    def open_location_force_new_window (self, location_uri, selection):
        _nautilus.nautilus_view_open_location_force_new_window (self._o, location_uri, selection)

    def report_location_change (self, location_uri, selection, title):
	_nautilus.nautilus_view_report_location_change (self._o, location_uri, selection, title)
	
    def report_redirect (self, from_location_uri, to_location_uri, selection, title):
	_nautilus.nautilus_view_report_redirect (self._o, from_location_uri, to_location_uri, selection, title)
	
    def report_selection_change (self, selection):
	_nautilus.nautilus_view_report_selction_change (self._o, selection)
	
    def report_status (self, selection):
	_nautilus.nautilus_view_report_status (self._o, status)
	
    def report_load_underway (self):
        _nautilus.nautilus_view_report_load_underway (self._o)
        
    def report_load_progress (self, fraction_done):
        _nautilus.nautilus_view_report_load_progress (self._o, fraction_done)

    def report_load_complete (self):
        _nautilus.nautilus_view_report_load_complete (self._o)

    def report_load_failed (self):
        _nautilus.nautilus_view_report_load_failed (self._o)
        
    def set_title (self, title):
        _nautilus.nautilus_view_set_tile (self._o, title)

    def go_back (self, title):
        _nautilus.nautilus_view_go_back (self._o)

    def set_up_ui (self, datadir, ui_xml_file_name, application_name):
        return bonobo.BonoboUIComponent (_obj=_nautilus.nautilus_view_set_up_ui
                                         (self._o, datadir, ui_xml_file_name, application_name))
        
def make_object (factory, object_id, klass):
    return klass ()
                 
def view_standard_main (factory_iid, klass):
    bonobo.BonoboGenericFactory (factory_iid, make_object, klass)
    bonobo.main ()
    
#    _nautilus.nautilus_view_standard_main (factory_iid, view_iid, klass, make_object)
