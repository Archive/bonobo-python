/* bonobo-python - Bonobo bindings for python.
 * Copyright (C) 2001 Johan Dahlin <zilch.am@home.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include "pybonobo.h"
#include <sysmodule.h>

static void            ListenerMarshal                (BonoboListener               *listener,
						       char                         *event_name,
						       CORBA_any                    *any,
						       CORBA_Environment            *ev,
						       PyObject                     *tuple);

static BonoboObject *  BonoboGenericFactoryMarshal    (BonoboGenericFactory         *Factory,
						       void                         *closure);

static BonoboObject *  GenericFactoryCallbackMarshal  (BonoboObject                 *factory,
						       const char                   *object_id,
						       PyObject                     *tuple);

static Bonobo_Unknown  MonikerExtenderMarshal         (BonoboMonikerExtender        *extender,
						       const Bonobo_Moniker         parent,
						       const Bonobo_ResolveOptions  *options,
						       const CORBA_char             *display_name,
						       const CORBA_char             *requested_interface,
						       CORBA_Environment            *ev);

static void            UIListenerMarshal              (BonoboUIComponent            *component,
						       const char                   *path,
						       Bonobo_UIComponent_EventType  type,
						       const char                   *state,
						       PyObject                     *tuple);

static void            UIVerbMarshal                  (BonoboUIComponent            *component,
						       PyObject                     *tuple,
						       const char                   *cname);

static PyObject* dict;

void
my_init_pygtk (void)
{
	/* this return void, so i avoid one warning by this :) */
	init_pygtk ();
	my_init_pygdkpixbuf ();
}

/* gnome_init ()
 * 
 */
static PyObject *
_wrap_gnome_init (PyObject *self,
		  PyObject *args)
{

	PyObject  *av;
	int        argc;
	int        i;
	char     **argv;
	char      *app_id;
	char      *app_version;
    
	if (!PyArg_ParseTuple (args, "ss:gnome_init",
			       &app_id, &app_version)) {
		return NULL;
	}

	av = PySys_GetObject ("argv");
	argc = PyList_Size (av);
	
	argv = malloc (argc * sizeof (char*));
	for (i = 0; i < argc; i++) {
		argv[i] = strdup (PyString_AsString (PyList_GetItem (av, i)));
	}
	Py_INCREF (av);

	gnomelib_register_popt_table (oaf_popt_options, "OAF");
	gnome_init (app_id, app_version, argc, argv);

	my_init_pygtk ();

	Py_INCREF (Py_None);
	return Py_None;
}

GList*
PySequence_ToGList (PyObject *object)
{
	GList  *list;
	int     size;
	int     i;
	gchar  *tmp;
    
	if (!PySequence_Check (object)) {
		return NULL;
	}

	list = NULL;
	size = PySequence_Length (object);
	for (i = 0; i < size; i++) {
		tmp = g_strdup (PyString_AsString (PySequence_GetItem (object, i)));
		list = g_list_append (list, tmp);
	}
    
	return list;
}

PyObject*
GList_ToPyList (GList *list)
{
	guint     size;
	PyObject *object;
	guint     i;

	size = g_list_length (list);
	object = PyList_New (size);
	
	for (i = 0; i < size; i++) {
		PyList_SetItem (object, i, PyString_FromString (g_list_nth_data (list, i)));
	}
	
	return object;
}

/* */
static PyObject*
PyBonoboUINode_New (BonoboUINode *node)
{
	PyBonoboUINode *self;
  
	self = (PyBonoboUINode *)PyObject_NEW(PyBonoboUINode, &PyBonoboUINode_Type);
	if (self == NULL) {
		return NULL;
	}
	
	self->obj = node;

	return (PyObject *)self;
}

static void
PyBonoboUINode_dealloc (PyBonoboUINode *self)
{
	PyMem_DEL(self);
}

static PyTypeObject PyBonoboUINode_Type = {
	PyObject_HEAD_INIT (NULL)
	0,                                    /* ob_size */
	"BonoboUINode",                       /* tp_name */
	sizeof (PyBonoboUINode),              /* tp_basicsize */
	0,                                    /* tp_itemsize */
	
	/* methods */
	(destructor)PyBonoboUINode_dealloc,   /* tp_dealloc */
	0,                                    /* tp_print */
	(getattrfunc)0,                       /* tp_getattr */
	(setattrfunc)0,                       /* tp_setattr */
	0,                                    /* tp_compare */
	0,                                    /* tp_repr */
	0,                                    /* tp_as_number */
	0,                                    /* tp_as_sequence */
	0,                                    /* tp_as_mapping */
	0,                                    /* tp_hash */
};

static PyObject *
PrepareArgs (PyObject *func,
	     PyObject *extra)
{
	if (!PyCallable_Check (func)) {
		PyErr_SetString (PyExc_TypeError, "first argument must be callable");
		return NULL;
	}
	
	if (extra != Py_None) {
		Py_INCREF (extra);
	} else {
		Py_INCREF (Py_None);
		extra = Py_None;
	}
	
	return Py_BuildValue ("(ON)", func, extra);		
	
}

static PyObject *
_wrap_bonobo_control_get_property (PyObject *self,
				   PyObject *args)
{
	PyGtk_Object  *widget;
	gchar         *format;
	PyObject      *property;
    
	if (!PyArg_ParseTuple (args, "O!s:bonboo_control_get_property",
			       &PyGtk_Type, &widget, &format)) {
		return NULL;
	}
	
	bonobo_control_get_property (BONOBO_CONTROL (PyGtk_Get (widget)), format, &property, NULL);

	return property;
}

static PyObject*
_wrap_bonobo_control_set_property (PyObject *self,
				   PyObject *args)
{
	PyGtk_Object *widget;
	PyObject     *property;
	gchar        *format;
	gpointer      data;
	
	if (!PyArg_ParseTuple (args, "O!sO:bonboo_control_set_property",
			       &PyGtk_Type, &widget, &format, &property)) {
		return NULL;
	}
	
	if (PyString_Check (property)) {
		bonobo_control_set_property (BONOBO_CONTROL (PyGtk_Get (widget)),
					     format,
					     PyString_AsString (property),
					     NULL);		
	} else if (PyInt_Check (property)) {
		bonobo_control_set_property (BONOBO_CONTROL (PyGtk_Get (widget)),
					     format,
					     PyInt_AsLong (property),
					     NULL);
	}


	Py_INCREF (Py_None);
	return Py_None;
}

/* Bonobo_EventSource_ListenerId bonobo_event_source_client_add_listener (Bonobo_Unknown object,
 *                                                                        BonoboListenerCallbackFn event_callback,
 *                                                                        const char *opt_mask,
 *                                                                        CORBA_Environment *opt_ev,
 *                                                                        gpointer user_data);
 */
static PyObject*
_wrap_bonobo_event_source_client_add_listener (PyObject *self,
					       PyObject *args)
{
	PyObject                      *object;
	PyObject                      *func;
	PyObject                      *extra;
	gchar                         *opt_mask;
	PyObject                      *data;
	CORBA_Environment              ev;
	Bonobo_EventSource_ListenerId  retval;

	if (!PyArg_ParseTuple (args, "OOsO:bonobo_event_source_client_add_listener",
			       &object, &func, &opt_mask, &extra)) {
		return NULL;
	}

	data = PrepareArgs (func, extra);
	if (data == NULL) {
		return NULL;
	}
	
	CORBA_exception_init (&ev);
	retval = bonobo_event_source_client_add_listener (PyORBit_Object_Get (object),
							  (BonoboListenerCallbackFn)ListenerMarshal,
							  opt_mask,
							  &ev,
							  data);
	CORBA_exception_free (&ev);
	
	return Py_BuildValue ("i", retval);
}


/* BonoboObject* (*BonoboGenericFactoryFn)     (BonoboGenericFactory *Factory,
 *                                              void *closure);
 */
static BonoboObject *
BonoboGenericFactoryMarshal (BonoboGenericFactory *Factory,
			     void                 *closure)
{
	PyObject *func, *iid, *userdata, *params;
	PyObject *tuple = closure;
	PyObject *object;
	BonoboObject *retval;
	
	if (PyTuple_Check (tuple)) {    
		func = PyTuple_GetItem (tuple, 0);
		userdata = PyTuple_GetItem (tuple, 1);
	} else {
		func = tuple;
	}
	
	if (!PyCallable_Check (func)) {
		PyErr_SetString (PyExc_TypeError, "Parameter must be callable");
		return NULL;
	}
	
	if (userdata != NULL) {
		params = PyTuple_New (2);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (Factory)));
		PyTuple_SetItem (params, 1, userdata);
		object = PyObject_CallObject (func, params);
		Py_DECREF (params);
		Py_DECREF (userdata);        
	} else {
		object = PyObject_CallObject (func, PyGtk_New (GTK_OBJECT (Factory)));
	}
	
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		// FIXME: Add a better 'killer'
		gtk_main_quit ();
		return NULL;
	}
	
	if (object == NULL) {
		g_message ("GnomeFactoryCallback must return a GtkObject.");
		
		gtk_main_quit ();
	}
	
	if (PyObject_HasAttrString (object, "_o")) {
		retval = BONOBO_OBJECT (PyGtk_Get (PyObject_GetAttrString(object, "_o")));
	} else {
		retval = BONOBO_OBJECT (PyGtk_Get (object));
	}
	bonobo_object_ref (retval);
    
	return retval;
}

/* BonoboObject* (*GnomeFactoryCallback) (BonoboGenericFactory *factory,
 *                                        const char           *component_id,
 *                                        gpointer              closure);
 */
static BonoboObject *
GenericFactoryCallbackMarshal (BonoboObject *factory,
			       const char   *object_id,
			       PyObject     *tuple)
{
	PyObject          *func;
	PyObject          *userdata;
	PyObject          *params;
	PyObject          *object;
	BonoboObject      *retval;

	if (PyTuple_Check (tuple)) {
		func = PyTuple_GetItem (tuple, 0);
		userdata = PyTuple_GetItem (tuple, 1);
	} else {
		func = tuple;
	}

	if (!PyCallable_Check (func)) {
		PyErr_SetString (PyExc_TypeError, "Parameter must be callable");
		return NULL;
	}

	if (userdata != Py_None) {
		params = PyTuple_New (3);        
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (factory)));
		PyTuple_SetItem (params, 1, Py_BuildValue ("s", object_id));
		PyTuple_SetItem (params, 2, userdata);
		object = PyObject_CallObject (func, params);
		Py_DECREF (params);
	} else {
		params = PyTuple_New (2);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (factory)));
		PyTuple_SetItem (params, 1, Py_BuildValue ("s", object_id));
		object = PyObject_CallObject (func, params);
		Py_DECREF (params);        
		Py_DECREF (userdata);
	}
	
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		// FIXME: Add a better 'killer'
		gtk_main_quit();
		return NULL;
	}
	
	if (object == NULL) {
		g_message ("GnomeFactoryCallback must return a GtkObject.");
		gtk_main_quit ();
	}
	
	if (PyObject_HasAttrString (object, "_o")) {
		retval = BONOBO_OBJECT (PyGtk_Get (PyObject_GetAttrString (object, "_o")));
	} else {
		retval = BONOBO_OBJECT (PyGtk_Get (object));
	}
	
	bonobo_object_ref (retval);
	
	return retval;
}

/* BonoboGenericFactory* bonobo_generic_factory_new (const char             *oaf_iid,
 *                                                   BonoboGenericFactoryFn  factory,
 *                                                   gpointer                user_data);
 */
static PyObject*
_wrap_bonobo_generic_factory_new (PyObject *self,
				  PyObject *args)
{
	char                 *iid;
	PyObject             *func;
	PyObject             *userdata;
	PyObject             *data;
	BonoboGenericFactory *factory;

	userdata = NULL;
	if (!PyArg_ParseTuple (args, "sO|O:bonobo_generic_factory_new",
			       &iid, &func, &userdata)) {
		return NULL;
	}
	
	data = PrepareArgs (func, userdata);
	if (data == NULL) {
		return NULL;
	}

	factory = bonobo_generic_factory_new (iid,
					      (BonoboGenericFactoryFn)BonoboGenericFactoryMarshal,
					      data);
	
	bonobo_running_context_auto_exit_unref (BONOBO_OBJECT (factory));
	
	return PyGtk_New (GTK_OBJECT (factory));
}

/* BonoboGenericFactory* bonobo_generic_factory_new_multi (const char              *oaf_iid,
 *                                                         BonoboGenericFactoryFn   factory,
 *                                                         gpointer                 user_data);
 */
static PyObject*
_wrap_bonobo_generic_factory_new_multi (PyObject *self,
					PyObject *args)
{
	char                 *iid;
	PyObject             *func;
	PyObject             *userdata;
	PyObject             *data;
	BonoboGenericFactory *factory;
	
	userdata = NULL;
	if (!PyArg_ParseTuple (args, "sOO:bonobo_generic_factory_new_multi",
			       &iid, &func, &userdata)) {
		return NULL;
	}

	data = PrepareArgs (func, userdata);
	if (data == NULL) {
		return NULL;
	}
	
	factory = bonobo_generic_factory_new_multi (iid,
						    (GnomeFactoryCallback)GenericFactoryCallbackMarshal,
						    data);
	
	bonobo_running_context_auto_exit_unref (BONOBO_OBJECT (factory));
	
	return PyGtk_New (GTK_OBJECT (factory));
}

/* gboolean bonobo_init (CORBA_ORB orb, PortableServer_POA poa, PortableServer_POAManager manager)
 *                                                                 
 */                                                                
static PyObject*
_wrap_bonobo_init (PyObject *self,
		   PyObject *args)   
{                                                                  
	CORBA_ORB_PyObject        *orb;
	POA_PyObject              *poa;
	POAManager_PyObject       *manager;
	PortableServer_POA         real_poa;
	PortableServer_POAManager  real_poamanager;
	gboolean                   retval;
	
	poa = NULL;
	manager = NULL;
	if (!PyArg_ParseTuple (args, "O!|O!O!:bonobo_init",
			       &PyORBit_ORB_Type, &orb,
			       &PyORBit_POA_Type, &poa,
			       &PyORBit_POAManager_Type, &manager)) {
		return NULL;
	}
	
	if (poa != NULL) {
		real_poa = PyORBit_POA_Get (poa);
	} else {
		real_poa = CORBA_OBJECT_NIL;
	}

	if (manager != NULL) {
		real_poamanager = PyORBit_POAManager_Get (manager);
	} else {
		real_poamanager = CORBA_OBJECT_NIL;
	}
	
	retval = bonobo_init (PyORBit_ORB_Get (orb), real_poa, real_poamanager);
                                                                   
	return Py_BuildValue ("b", retval);                                                       
}

/* void (*BonoboListenerCallbackFn)  (BonoboListener    *listener,
 *                                    char              *event_name, 
 *                                    CORBA_any         *any,
 *                                    CORBA_Environment *ev,
 *                                    gpointer           user_data);
 */
void
ListenerMarshal (BonoboListener    *listener,
		 char              *event_name,
		 CORBA_any         *any,
		 CORBA_Environment *ev,
		 PyObject          *tuple)
{
	PyObject *func;
	PyObject *userdata;
	PyObject *any_py;
	PyObject *params;
 	PyObject *object;

	func = PyTuple_GetItem (tuple, 0);
	userdata = PyTuple_GetItem (tuple, 1);
	
	if (!PyCallable_Check (func)) {
		PyErr_SetString(PyExc_TypeError, "Parameter must be callable");
		return;
	}

	any_py = (PyObject*)PyORBit_Any_New (any);
	Py_INCREF (any_py);
		   
	if (userdata == Py_None) {
		params = PyTuple_New (3);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (listener)));
		PyTuple_SetItem (params, 1, PyString_FromString (event_name));
		PyTuple_SetItem (params, 2, any_py);		
	} else {
		params = PyTuple_New (4);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (listener)));
		PyTuple_SetItem (params, 1, PyString_FromString (event_name));
		PyTuple_SetItem (params, 2, any_py);
		PyTuple_SetItem (params, 3, userdata);
		Py_DECREF (userdata);
	}

	object = PyObject_CallObject (func, params);
	Py_DECREF (params);

	if (PyErr_Occurred ()) {
		PyErr_Print ();
		gtk_main_quit();
		return;
	}

	if (object == NULL) {
		g_message ("GnomeFactoryCallback must return a GtkObject.");
		gtk_main_quit ();
	}
}

/* BonoboListener *bonobo_listener_new       (BonoboListenerCallbackFn event_callback, 
 *                                            gpointer                 user_data);
 */
static PyObject*
_wrap_bonobo_listener_new (PyObject *self,
			   PyObject *args)
{
	PyObject *func, *data, *extra = NULL;
	
	if (!PyArg_ParseTuple (args, "OO:bonobo_listener_new",
			       &func, &extra)) {
		return NULL;
	}
	
	data = PrepareArgs (func, extra);
	if (data == NULL) {
		return NULL;
	}
	
	return PyGtk_New (GTK_OBJECT (bonobo_listener_new ((BonoboListenerCallbackFn)ListenerMarshal, data)));
}

/* Bonobo_Unknown (*BonoboMonikerExtenderFn) (BonoboMonikerExtender       *extender,
 *                                            const Bonobo_Moniker         parent,
 *                                            const Bonobo_ResolveOptions *options,
 *                                            const CORBA_char            *display_name,
 *                                            const CORBA_char            *requested_interface,
 *                                            CORBA_Environment           *ev);
 */
static Bonobo_Unknown
MonikerExtenderMarshal (BonoboMonikerExtender       *extender,
			const Bonobo_Moniker         parent,
			const Bonobo_ResolveOptions *options,
			const CORBA_char            *display_name,
			const CORBA_char            *requested_interface,
			CORBA_Environment           *ev)
{
	PyObject *ret_object;
	PyObject *func, *userdata = NULL, *params;
	PyObject *tuple = extender->data;
	BonoboXObject object = extender->object;
        
	if (PyTuple_Check (tuple)) {    
		func = PyTuple_GetItem (tuple, 0);
		userdata = PyTuple_GetItem (tuple, 1);
	} else {
		func = tuple;
	}
	
	if (!PyCallable_Check (func)) {
		PyErr_SetString (PyExc_TypeError, "Parameter must be callable");
		return;
	}
	
	if (userdata != Py_None) {
		params = PyTuple_New (5);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (&object)));
		PyTuple_SetItem (params, 1, PyORBit_Object_New (CORBA_Object_duplicate (parent, ev)));
//        PyTuple_SetItem(params, 2, PyORBit_Object_New (CORBA_Object_duplicate (resolv, ev)));
		PyTuple_SetItem (params, 2, Py_BuildValue ("s", display_name));
		PyTuple_SetItem (params, 3, Py_BuildValue ("s", requested_interface));        
		PyTuple_SetItem (params, 4, userdata);
		ret_object = PyObject_CallObject (func, params);
		Py_DECREF (params);
	} else {
		params = PyTuple_New (4);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (&object)));
		PyTuple_SetItem (params, 1, PyORBit_Object_New (CORBA_Object_duplicate (parent, ev)));
//        PyTuple_SetItem(params, 2, PyORBit_Object_New (CORBA_Object_duplicate (resolv, ev)));
		PyTuple_SetItem (params, 2, Py_BuildValue ("s", display_name));
		PyTuple_SetItem (params, 3, Py_BuildValue ("s", requested_interface));        
		ret_object = PyObject_CallObject (func, params);
		Py_DECREF (params);
	}
    
	if (userdata != NULL) {
		Py_DECREF (userdata);
	}
    
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		// FIXME: Add a better 'killer'
		gtk_main_quit ();
		return;
	}
}

/* BonoboMonikerExtender *bonobo_moniker_extender_new      (BonoboMonikerExtenderFn      resolve,
 *                                                          gpointer                     data);
 */
static PyObject*
_wrap_bonobo_moniker_extender_new (PyObject *self,
				   PyObject *args)
{
	PyObject *func, *data, *extra = NULL;
	gchar    *name;
    
	if (!PyArg_ParseTuple (args, "OO:bonobo_moniker_extender_new",
			       &func, &extra)) {
		return NULL;
	}
    
	data = PrepareArgs (func, extra);
	if (data == NULL) {
		return NULL;
	}

	bonobo_moniker_extender_new ((BonoboMonikerExtenderFn)MonikerExtenderMarshal,
				     data);
    
	Py_INCREF (Py_None);
	return Py_None;    
}

static PyObject*
_wrap_bonobo_stream_mem_create (PyObject* self,
				PyObject* args)
{
	size_t        size;
	gboolean      read_only;
	gboolean      resizable;
	BonoboStream *retval;

	if (!PyArg_ParseTuple (args, "lbb:bonobo_stream_mem_create",
			       &size, &read_only, &resizable)) {
		return NULL;
	}
	
	retval = BONOBO_STREAM (bonobo_stream_mem_create (NULL, size, read_only, resizable));

	return PyGtk_New (GTK_OBJECT (retval));
}

/* void        (*BonoboUIVerbFn)               (BonoboUIComponent *component,
 *                                              gpointer user_data, const char *cname)
 */
static void
UIVerbMarshal (BonoboUIComponent *component,
	       PyObject          *tuple,
	       const char        *cname)
{
	PyObject *object;
	PyObject *func;
	PyObject *userdata;
	PyObject *params;
	
	if (PyTuple_Check (tuple)) {    
		func = PyTuple_GetItem (tuple, 0);
		userdata = PyTuple_GetItem (tuple, 1);
	} else {
		func = tuple;
	}
	
	if (!PyCallable_Check (func)) {
		PyErr_SetString(PyExc_TypeError, "Parameter must be callable");
		return;
	}
	
	if (userdata == Py_None) {
		params = PyTuple_New (2);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (component)));
		PyTuple_SetItem (params, 1, Py_BuildValue ("s", cname));
		object = PyObject_CallObject (func, params);
		Py_DECREF (params);
	} else {
		params = PyTuple_New (3);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (component)));
		PyTuple_SetItem (params, 1, Py_BuildValue ("s", cname));
		PyTuple_SetItem (params, 2, userdata);
		object = PyObject_CallObject (func, params);
		Py_DECREF (params);
		Py_DECREF (userdata);
	}
	
	if (PyErr_Occurred()) {
		PyErr_Print ();
		// FIXME: Add a better 'killer'
		gtk_main_quit();
		return;
	}
}

/* void        (*BonoboUIListenerFn)           (BonoboUIComponent            *component,
 *                                              const char                   *path,
 *                                              Bonobo_UIComponent_EventType  type,
 *                                              const char                   *state,
 *                                              gpointer                      user_data);
 */
static void
UIListenerMarshal (BonoboUIComponent            *component,
		   const char                   *path,
		   Bonobo_UIComponent_EventType  type,
		   const char                   *state,
		   PyObject                     *tuple)
{
	PyObject *func;
	PyObject *userdata;
	PyObject *params;
	PyObject *object;
	
	func = PyTuple_GetItem (tuple, 0);
	userdata = PyTuple_GetItem (tuple, 1);
	
	if (!PyCallable_Check (func)) {
		PyErr_SetString(PyExc_TypeError, "Parameter must be callable");
		return;
	}
	
	if (userdata != Py_None) {
		params = PyTuple_New (5);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (component)));
		PyTuple_SetItem (params, 1, Py_BuildValue ("s", path));
		PyTuple_SetItem (params, 2, Py_BuildValue ("i", type));		
		PyTuple_SetItem (params, 3, Py_BuildValue ("s", state));		
		PyTuple_SetItem (params, 4, userdata);
		Py_INCREF (userdata);
		Py_INCREF (params);		
		
	} else {
		params = PyTuple_New (4);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (component)));
		PyTuple_SetItem (params, 1, Py_BuildValue ("s", path));
		PyTuple_SetItem (params, 2, Py_BuildValue ("i", type));
		PyTuple_SetItem (params, 3, Py_BuildValue ("s", state));
	}
	
	object = PyObject_CallObject (func, params);
	Py_DECREF (params);
	if (userdata != Py_None) {
		Py_XDECREF (userdata);
	}
	
	if (PyErr_Occurred()) {
		PyErr_Print ();
		// FIXME: Add a better 'killer'
		gtk_main_quit();
		return;
	}
}

/* void        bonobo_ui_component_add_listener (BonoboUIComponent *component,
 *                                               const char        *id,
 *                                               BonoboUIListenerFn fn,
 *                                               gpointer           user_data);
 */
static PyObject*
_wrap_bonobo_ui_component_add_listener (PyObject *self,
					PyObject *args)
{
	PyGtk_Object *component;
	gchar        *id;
	PyObject     *fn;
	PyObject     *user_data;
	PyObject     *data;

	user_data = NULL;
	if (!PyArg_ParseTuple (args, "O!sOO:bonobo_ui_component_add_listener",
			       &PyGtk_Type, &component, &id, &fn, &user_data)) {
		return NULL;
	}
	
	data = PrepareArgs (fn, user_data);
	if (data == NULL) {
		return NULL;
	}

	bonobo_ui_component_add_listener (BONOBO_UI_COMPONENT (PyGtk_Get (component)),
					  id,
					  (BonoboUIListenerFn)UIListenerMarshal,
					  data);
	
	Py_INCREF (Py_None);
	return Py_None;    
}

static void 
PropertyGetMarshal (BonoboPropertyBag *bag,
		    BonoboArg         *arg,   
		    guint              arg_id,
		    CORBA_Environment *ev,
		    PyObject          *tuple)
{
	PyObject *func;
	PyObject *userdata;
	PyObject *params;
	PyObject *object;
	
	func = PyTuple_GetItem (tuple, 0);
	userdata = PyTuple_GetItem (tuple, 2);
	
	if (!PyCallable_Check (func)) {
		PyErr_SetString(PyExc_TypeError, "Parameter must be callable");
		return;
	}
	
	if (userdata != Py_None) {
		params = PyTuple_New (4);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (bag)));
		PyTuple_SetItem (params, 1, (PyObject*)PyORBit_Any_New (arg));
		PyTuple_SetItem (params, 2, Py_BuildValue ("i", arg_id));		
		PyTuple_SetItem (params, 3, userdata);
		
	} else {
		params = PyTuple_New (3);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (bag)));
		PyTuple_SetItem (params, 1, (PyObject*)PyORBit_Any_New (arg));
		PyTuple_SetItem (params, 2, Py_BuildValue ("i", arg_id));		
	}
	
	object = PyObject_CallObject (func, params);
	Py_DECREF (params);
	if (userdata != Py_None) {
		Py_XDECREF (userdata);
	}
	
	if (PyErr_Occurred()) {
		PyErr_Print ();
		gtk_main_quit();
		return;
	}

	if (PyORBit_Any_Check (object)) {
		arg = (CORBA_any*)PyORBit_Any_Get ((CORBA_Any_PyObject*)object);
	}
}

static void 
PropertySetMarshal (BonoboPropertyBag *bag,
		    const BonoboArg   *arg,   
		    guint              arg_id,
		    CORBA_Environment *ev,
		    PyObject          *tuple)
{
	PyObject *func;
	PyObject *userdata;
	PyObject *params;
	PyObject *object;
	
	func = PyTuple_GetItem (tuple, 1);
	userdata = PyTuple_GetItem (tuple, 2);
	
	if (!PyCallable_Check (func)) {
		PyErr_SetString(PyExc_TypeError, "Parameter must be callable");
		return;
	}
	
	if (userdata != Py_None) {
		params = PyTuple_New (4);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (bag)));
		PyTuple_SetItem (params, 1, (PyObject*)PyORBit_Any_New ((BonoboArg*)arg));
		PyTuple_SetItem (params, 2, Py_BuildValue ("i", arg_id));		
		PyTuple_SetItem (params, 3, userdata);
		
	} else {
		params = PyTuple_New (3);
		PyTuple_SetItem (params, 0, PyGtk_New (GTK_OBJECT (bag)));
		PyTuple_SetItem (params, 1, (PyObject*)PyORBit_Any_New ((BonoboArg*)arg));
		PyTuple_SetItem (params, 2, Py_BuildValue ("i", arg_id));		
	}
	
	PyObject_CallObject (func, params);
	Py_DECREF (params);
	if (userdata != Py_None) {
		Py_XDECREF (userdata);
	}
	
	if (PyErr_Occurred()) {
		PyErr_Print ();
		gtk_main_quit();
		return;
	}
}


/* BonoboPropertyBag        *bonobo_property_bag_new             (BonoboPropertyGetFn get_prop,
 *                                                                BonoboPropertySetFn set_prop,
 *                                                                gpointer            user_data);
 */
static PyObject*
_wrap_bonobo_property_bag_new (PyObject *self,
			       PyObject *args)
{
	PyObject          *get_prop;
	PyObject          *set_prop;
	PyObject          *extra;	
	PyObject          *user_data;
	PyObject          *data;
	void              *get_prop_marshal;
	void              *set_prop_marshal;
	BonoboPropertyBag *property_bag;
	
	if (!PyArg_ParseTuple (args, "OOO:bonobo_property_bag_new",
			       &get_prop, &set_prop, &extra)) {
		return NULL;
	}
	
	get_prop_marshal = PyCallable_Check (get_prop) ?
		PropertyGetMarshal : NULL;
	
	set_prop_marshal = PyCallable_Check (get_prop) ?
		PropertySetMarshal : NULL;	
	
	if (extra != Py_None) {
		Py_INCREF (extra);
	} else {
		Py_INCREF (Py_None);
		extra = Py_None;
	}
	
	data = Py_BuildValue ("(OON)", get_prop, set_prop, extra);
 
	property_bag = BONOBO_PROPERTY_BAG (
		bonobo_property_bag_new ((BonoboPropertyGetFn)get_prop_marshal,
					 (BonoboPropertySetFn)set_prop_marshal,
					 data));
	
	return PyGtk_New (GTK_OBJECT (property_bag));
}

/* void      bonobo_property_bag_add   (BonoboPropertyBag   *PropertyBag,
 *                                      const gchar         *name,
 *					int                  idx,
 *					BonoboArgType        type,
 *					BonoboArg           *default_value, 
 *					const gchar         *docstring,
 *					BonoboPropertyFlags  flags)
 *
 */
static PyObject *
_wrap_bonobo_property_bag_add (PyObject* self,
			       PyObject* args)
{
        PyGtk_Object            *PropertyBag;
        const gchar             *name;
        int                      idx;
        PyORBit_TypeCode        *type;
        PyORBit_Any             *default_value;
        const gchar             *docstring;
        BonoboPropertyFlags      flags;
	BonoboArg               *real_default_value;

        if (!PyArg_ParseTuple (args, "O!siO!Ozi:bonobo_property_bag_add",
			       &PyGtk_Type, &PropertyBag,
			       &name,
			       &idx,
			       &PyORBit_TypeCode_Type, &type,
			       &default_value,
			       &docstring,
			       &flags)) {
		return NULL;
	}

	real_default_value = PyORBit_Any_Check (default_value) ?
		PyORBit_Any_Get (default_value) : NULL;
		
        bonobo_property_bag_add (BONOBO_PROPERTY_BAG (PyGtk_Get (PropertyBag)),
				 name,
				 idx,
				 PyORBit_TypeCode_Get (type),
				 real_default_value,
				 docstring,
				 flags);
				 
        Py_INCREF (Py_None);
        return Py_None;
}

/* void bonobo_ui_component_add_verb (BonoboUIComponent *component, const char *cname,
 *                                    BonoboUIVerbFn fn, gpointer user_data);
 */
static PyObject*
_wrap_bonobo_ui_component_add_verb (PyObject *self,
				    PyObject *args)
{
	PyGtk_Object *component;
	PyObject *func, *data, *extra = NULL;
	gchar *cname;
    
	if (!PyArg_ParseTuple (args, "O!sOO:bonobo_ui_component_add_verb",
			       &PyGtk_Type, &component, &cname, &func, &extra)) {
		return NULL;
	}
	
	data = PrepareArgs (func, extra);
	if (data == NULL) {
		return NULL;
	}

	bonobo_ui_component_add_verb (BONOBO_UI_COMPONENT (PyGtk_Get (component)),
				      cname,
				      (BonoboUIVerbFn)UIVerbMarshal,
				      data);
	
	Py_INCREF (Py_None);
	return Py_None;    
}

static PyObject*
_wrap_bonobo_widget_get_property (PyObject *self,
				  PyObject *args)
{
	PyGtk_Object  *widget;
	gchar         *format;
	PyObject      *property;
    
	if (!PyArg_ParseTuple (args, "O!s:bonboo_widget_get_property",
			       &PyGtk_Type, &widget, &format)) {
		return NULL;
	}
	
	bonobo_widget_get_property (BONOBO_WIDGET (PyGtk_Get (widget)), format, &property, NULL);

	return property;
}

static PyObject*
_wrap_bonobo_widget_set_property (PyObject *self,
				  PyObject *args)
{
	PyGtk_Object *widget;
	PyObject     *property;
	gchar        *format;
	
	if (!PyArg_ParseTuple (args, "O!sO:bonboo_widget_set_property",
			       &PyGtk_Type, &widget, &format, &property)) {
		return NULL;
	}

	bonobo_widget_set_property (BONOBO_WIDGET (PyGtk_Get (widget)), format, property, NULL);
	
	Py_INCREF (Py_None);
	return Py_None;
}

        
#include "bonobomodule_impl.c"

/* Methods */
static struct PyMethodDef bonobomod_methods[] =
{
#include "bonobomodule_defs.c"
	{ "bonobo_control_set_property",       _wrap_bonobo_control_set_property,     METH_VARARGS },
	{ "bonobo_control_get_property",       _wrap_bonobo_control_get_property,     METH_VARARGS },        
	{ "bonobo_event_source_client_add_listener", _wrap_bonobo_event_source_client_add_listener,  METH_VARARGS },    
	{ "bonobo_generic_factory_new",       _wrap_bonobo_generic_factory_new,       METH_VARARGS },
	{ "bonobo_generic_factory_new_multi", _wrap_bonobo_generic_factory_new_multi, METH_VARARGS },
	{ "bonobo_init",                      _wrap_bonobo_init,                      METH_VARARGS },
	{ "bonobo_listener_new",              _wrap_bonobo_listener_new,              METH_VARARGS },
	{ "bonobo_moniker_extender_new",      _wrap_bonobo_moniker_extender_new,      METH_VARARGS },
	{ "bonobo_property_bag_new",          _wrap_bonobo_property_bag_new,          METH_VARARGS },
	{ "bonobo_property_bag_add",          _wrap_bonobo_property_bag_add,          METH_VARARGS },		
        { "bonobo_stream_mem_create",         _wrap_bonobo_stream_mem_create,         METH_VARARGS },
	{ "bonobo_ui_component_add_listener", _wrap_bonobo_ui_component_add_listener, METH_VARARGS },
	{ "bonobo_ui_component_add_verb",     _wrap_bonobo_ui_component_add_verb,     METH_VARARGS },
	{ "bonobo_widget_set_property",       _wrap_bonobo_widget_set_property,       METH_VARARGS },
	{ "bonobo_widget_get_property",       _wrap_bonobo_widget_get_property,       METH_VARARGS },        
	{ "gnome_init",                       _wrap_gnome_init,                       METH_VARARGS },
	
	{ NULL, NULL }
};

void
init_bonobo (void)
{
	PyObject *bonobomod;
    
	init_orbit_python ();
    
	bonobomod = Py_InitModule ("_bonobo", bonobomod_methods);
	dict = PyModule_GetDict (bonobomod);

	if (PyErr_Occurred ()) {
		Py_FatalError ("Can't initialise module _bonobo\n");
	}
}
