# bonobo-python - Python bindings for bonobo.
# Copyright (C) 2001 Johan Dahlin <zilch.am@home.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author: Johan Dahlin
#

import sys
import os

import oaf
import _bonobo

import BONOBO

os.environ["DISABLE_CRASH_DIALOG"] = "1"

if "_gtk" in sys.modules.keys ():
    print "You must import bonobo before gtk."
    raise SystemExit

# This is a temporary hack.
# Call gnome_init and gnomelib_register_popt_table (with oaf_options)
# It's needed to register the correct command line arguments for OAF,
# which bonobo uses when it launches a bonobo component
_bonobo.gnome_init (sys.argv[0], "0.1")

# Register GnomeCanvasPoints & GdkImlibImage
import _gnomeui
_gnomeui._register_types ()

# Tell gnome-python that it's already initialized
import gnome
gnome.gnomelib_init_called = 1
gnome.gnome_init_called = 1

import _gtk
import gtk
import gnome.ui

_obj2inst = gtk._obj2inst

# Initialize oaf & bonobo
orb = oaf.init ()
_bonobo.bonobo_init (orb)

# bonobo-object
class BonoboObject (gtk.GtkObject):
    get_type = _bonobo.bonobo_object_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_object_from_servant (servant)
        self._o = _bonobo.bonobo_object_new_from_servant (servant)
#        self._o = _bonobo.bonobo_object_init ()

#    def bind_to_servant (self, servant):
#        return _bonobo.bonobo_object_bind_to_servant (self._o, servant)

#    def get_servant (self):
#        return _bonobo.bonobo_object_get_servant (self._o)

#    def activate_servant_full (self, servant, shlib_id):
#        return _bonobo.bonobo_object_activate_servant_full (self._o, servant, shlib_id)

    def activate_servant (self, servant):
        return _bonobo.bonobo_object_activate_servant (self._o, servant._o)

    def add_interface (self, newobj):
        return _bonobo.bonobo_object_add_interface (self._o, newobj)

    def query_interface (self, repo_id):
        return _bonobo.bonobo_object_query_interface (self._o, repo_id)

    def query_local_interface (self, repo_id):
        return _obj2inst (_bonobo.bonobo_object_query_local_interface (self._o, repo_id))

    def corba_objref (self):
        return _bonobo.bonobo_object_corba_objref (self._o)

    def ref (self):
        _bonobo.bonobo_object_ref (self._o)

    def idle_unref (self):
        _bonobo.bonobo_object_idle_unref (self._o)

    def unref (self):
        _bonobo.bonobo_object_unref (self._o)

    def trace_refs (self, fn, line, ref):
        _bonobo.bonobo_object_trace_refs (self._o, fn, line, ref)

    def dump_interfaces (self):
        _bonobo.bonobo_object_dump_interfaces (self._o)

    def check_env (self, corba_object):
        _bonobo.bonobo_object_check_env (self._o, corba_object)
gtk._name2cls['BonoboObject'] = BonoboObject

def object_dup_ref (object):
    return _bonobo.bonobo_object_dup_ref (object)

def object_release_unref (object):
    return _bonobo.bonobo_object_release_unref (object)

def unknown_ping (object):
    return _bonobo.bonobo_unknown_ping (object)

# bonobo-xobject
class BonoboXObject (BonoboObject):
    def __init__(self, _obj=None):
        if _obj: self._o = _obj; return
gtk._name2cls['BonoboXObject'] = BonoboXObject

# bonobo-arg
#self._o = _bonobo.bonobo_arg_type_from_gtk (t)
#self._o = _bonobo.bonobo_arg_to_gtk (a, arg)

#def bonobo_arg_is_equal (self, b):
#    return _bonobo.bonobo_arg_is_equal (self._o, b)

#def bonobo_arg_release (self):
#    return _bonobo.bonobo_arg_release (self._o)

#def bonobo_arg_from_gtk (self, arg):
#    return _bonobo.bonobo_arg_from_gtk (self._o, arg)

#def bonobo_arg_type_is_equal (self, b):
#    return _bonobo.bonobo_arg_type_is_equal (self._o, b)

#def bonobo_arg_copy (self):
#    return _bonobo.bonobo_arg_copy (self._o)

#def bonobo_arg_type_to_gtk (self):
#        return _bonobo.bonobo_arg_type_to_gtk (self._o)

# bonobo-async
#self._o = _bonobo.bonobo_async_handle_get_recv (reply)
#self._o = _bonobo.bonobo_async_demarshal (reply, retval, out_args)

# bonobo-canvas-component
class BonoboCanvasComponent (BonoboXObject):
    get_type = _bonobo.bonobo_canvas_component_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_canvas_component_new (item)

    def get_ui_container (self):
        return _bonobo.bonobo_canvas_component_get_ui_container (self._o)

    def get_item (self):
        return _obj2inst (_bonobo.bonobo_canvas_component_get_item (self._o))

    def grab (self, mask, cursor, time):
        _bonobo.bonobo_canvas_component_grab (self._o, mask, cursor._o, time)
        
    def ungrab (self, time):
        _bonobo.bonobo_canvas_component_ungrab (self._o, time)

    # set_bounds (component, bbox)

    # event (component, event)
    # return
gtk._name2cls['BonoboCanvasComponent'] = BonoboCanvasComponent

def canvas_new (is_aa, proxy):
    return _obj2inst (_bonobo.bonobo_canvas_new (is_aa, proxy))

# bonobo-canvas-item
class BonoboCanvasItem (gnome.ui.GnomeCanvasItem):
   get_type = _bonobo.bonobo_canvas_item_get_type
   def __init__ (self, _obj=None):
       if _obj:
           self._o = _obj;
           return
       else:
           print "Error"
   def set_bounds (self, x1, y1, x2, y2):
       _bonobo.bonobo_canvas_item_set_bounds (self._o, x1, y1, x2, y2)
gtk._name2cls['BonoboCanvasItem'] = BonoboCanvasItem

# bonobo-client-site
class BonoboClientSite (BonoboXObject):
    get_type = _bonobo.bonobo_client_site_get_type
    def __init__ (self, container=None, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_client_site_new (container._o)

    def bind_embeddable (self, object):
        return _bonobo.bonobo_client_site_bind_embeddable (self._o, object._o)

    def new_view_full (self, uic, visible_cover, active_view):
        return _obj2inst (_bonobo.bonobo_client_site_new_view_full (self._o, uic, visible_cover, active_view))

    def get_embeddable (self):
        return _obj2inst (_bonobo.bonobo_client_site_get_embeddable (self._o))

    def get_container (self):
        return _obj2inst (_bonobo.bonobo_client_site_get_container (self._o))

    def new_item (self, uic, group):
        return _obj2inst (_bonobo.bonobo_client_site_new_item (self._o, uic, group._o))

    def new_view (self, uic):
        return _obj2inst (_bonobo.bonobo_client_site_new_view (self._o, uic))

    def get_verbs (self):
        return _bonobo.bonobo_client_site_get_verbs (self._o)
gtk._name2cls['BonoboClientSite'] = BonoboClientSite

# bonobo-context
def context_get (context_name):
    return _bonobo.bonobo_context_get (context_name)

def context_add (context_name, context):
    _bonobo.bonobo_context_add (context_name, context)

def context_running_get ():
    return _obj2inst (_bonobo.bonobo_context_running_get ())

def running_context_auto_exit_unref (object):
    return _bonobo.bonobo_running_context_auto_exit_unref (object._o)

def context_init ():
    return _bonobo.bonobo_context_init ()

def context_shutdown ():
    return _bonobo.bonobo_context_shutdown ()

# bonobo-control
class BonoboControl (BonoboXObject):
    get_type = _bonobo.bonobo_control_get_type
    def __init__ (self, widget=None, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_control_new (widget._o)

    def get_widget (self):
        return _obj2inst (_bonobo.bonobo_control_get_widget (self._o))
    
    def set_automerge (self, automerge):
        _bonobo.bonobo_control_set_automerge (self._o, automerge)

    def get_automerge (self):
        return _bonobo.bonobo_control_get_automerge (self._o)

    def set_property (self, *args):
        no = len (args)
        if not no or no % 2:
            return
        
        for i in range (0, no, 2):
            if type (args[i]) != type (""):
                return
	    _bonobo.bonobo_control_set_property (self._o, args[i], args[i+1])

    def get_property (self, first_prop):
        _bonobo.bonobo_control_get_property (self._o, first_prop)

    def get_ui_component (self):
        return _obj2inst (_bonobo.bonobo_control_get_ui_component (self._o))

    def set_ui_component (self, component):
        _bonobo.bonobo_control_set_ui_component (self._o, component._o)

    def get_remote_ui_container (self):
        return _bonobo.bonobo_control_get_remote_ui_container (self._o)

    def set_control_frame (self, control_frame):
        _bonobo.bonobo_control_set_control_frame (self._o, control_frame)

    def control_frame (self):
        return _bonobo.bonobo_control_get_control_frame (self._o)

    def set_properties (self, pb):
        _bonobo.bonobo_control_set_properties (self._o, pb._o)

    def get_properties (self):
        return _obj2inst (_bonobo.bonobo_control_get_properties (self._o))

    def get_ambient_properties (self):
        return _bonobo.bonobo_control_get_ambient_properties (self._o)

    def activate_notify (self, activated):
        return _bonobo.bonobo_control_activate_notify (self._o, activated)
gtk._name2cls['BonoboControl'] = BonoboControl

def control_windowid_from_x11 (x11_id):
    return _bonobo.bonobo_control_windowid_from_x11 (x11_id)

# bonobo-control-frame
class BonoboControlFrame:
    get_type = _bonobo.bonobo_control_frame_get_type
    def __init__ (self, ui_container=None, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_control_frame_new (ui_container)

    def _get_widget (self):
        return _obj2inst (_bonobo.bonobo_control_frame_get_widget (self._o))

    def set_ui_container (self, uic):
        return _bonobo.bonobo_control_frame_set_ui_container (self._o, uic)

    def control_activate (self):
        _bonobo.bonobo_control_frame_control_activate (self._o)

    def control_deactivate (self):
        _bonobo.bonobo_control_frame_control_deactivate (self._o)

    def set_autoactivate (self, autoactivate):
        _bonobo.bonobo_control_frame_set_autoactivate (self._o, autoactivate)

    def get_autoactivate (self):
        return _bonobo.bonobo_control_frame_get_autoactivate (self._o)

    def get_control_property_bag (self):
        return _bonobo.bonobo_control_frame_get_control_property_bag (self._o)

    def set_propbag (self, propbag):
        _bonobo.bonobo_control_frame_set_propbag (self._o, propbag._o)

    def get_propbag (self):
        return _obj2inst (_bonobo.bonobo_control_frame_get_propbag (self._o))

    def control_set_state (self, state):
        _bonobo.bonobo_control_frame_control_set_state (self._o, state)

    def set_autostate (self, autostate):
        _bonobo.bonobo_control_frame_set_autostate (self._o, autostate)

    def get_autostate (self):
        return _bonobo.bonobo_control_frame_get_autostate (self._o)

    def bind_to_control (self, control):
        _bonobo.bonobo_control_frame_bind_to_control (self._o, control)

    def get_control (self):
        return _bonobo.bonobo_control_frame_get_control (self._o)

    def get_ui_container (self):
        return _bonobo.bonobo_control_frame_get_ui_container (self._o)

    def size_request (self, desired_width, desired_height):
        _bonobo.bonobo_control_frame_size_request (self._o, desired_width, desired_height)
        
    def sync_realize (self):
        _bonobo.bonobo_control_frame_sync_realize (self._o)

    def sync_unrealize (self):
        _bonobo.bonobo_control_frame_sync_unrealize (self._o)

    def focus_child (self, direction):
        return _bonobo.bonobo_control_frame_focus_child (self._o, direction)
gtk._name2cls['BonoboControlFrame'] = BonoboControlFrame

# bonobo-desktop-window
class BonoboDesktopWindow:
    get_type = _bonobo.bonobo_desktop_window_get_type
    def __init__ (self, window=None, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_desktop_window_new (window)
gtk._name2cls['BonoboDesktopWindow'] = BonoboDesktopWindow

def desktop_window_control (object, win):
    _bonobo.bonobo_desktop_window_control (object._o, win._o)

# bonobo-embeddable
class BonoboEmbeddable:
    get_type = _bonobo.bonobo_embeddable_get_type
    def __init__ (self, factory=None, data=None, _obj=None):
        if _obj: self._o = _obj; return
        callback = conv (factory)
        if type(factory) == BonoboViewFactory.get_type:
            self._o = _bonobo.bonobo_embeddable_new (callback.__call__, data)
        else:
            self._o = _bonobo.bonobo_embeddable_new_canvas_item (callback.__call__, data)

    def set_view_factory (self, factory, data):
        callback = conv(factory)
        return _bonobo.bonobo_embeddable_set_view_factory (self._o, callback.__call__, data)
    
    def get_uri (self):
        return _bonobo.bonobo_embeddable_get_uri (self._o)

    def set_uri (self, uri):
        _bonobo.bonobo_embeddable_set_uri (self._o, uri)

    def foreach_view (self, fn, data):
        callback = conv(fn)
        _bonobo.bonobo_embeddable_foreach_view (self._o, callback.__call__, data)

    def foreach_item (self, fn, data):
        callback = conv(fn)
        _bonobo.bonobo_embeddable_foreach_item (self._o, callback.__call__, data)
gtk._name2cls['BonoboEmbeddable'] = BonoboEmbeddable

# bonobo-event-source
class BonoboEventSource (BonoboXObject):
    get_type = _bonobo.bonobo_event_source_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_event_source_new ()

    def notify_listeners (self, event_name, value):
        _bonobo.bonobo_event_source_notify_listeners (self._o, event_name, value)

    def notify_listeners_full (self, path, type, subtype, value):
        return _bonobo.bonobo_event_source_notify_listeners_full (self._o, path, type, subtype, value)

    def ignore_listeners (self):
        return _bonobo.bonobo_event_source_ignore_listeners (self._o)
gtk._name2cls['BonoboEventSource'] = BonoboEventSource

def event_source_client_add_listener (object, event_callback, opt_mask, user_data=None):
    callback = conv (event_callback)
    _bonobo.bonobo_event_source_client_add_listener (object, callback.__call__, opt_mask, user_data)
    
def event_source_client_remove_listener (object, id):
    _bonobo.bonobo_event_source_client_remove_listener (object, id)


# bonobo-exception
#self._o = _bonobo.bonobo_exception_add_handler_str (repo_id, str)
#self._o = _bonobo.bonobo_exception_add_handler_fn (repo_id, fn, user_data, destroy_fn)
#self._o = _bonobo.bonobo_exception_repoid_to_text (repo_id)

# bonobo-generic-factory
class BonoboGenericFactory (BonoboObject):
    get_type = _bonobo.bonobo_generic_factory_get_type
    def __init__ (self, oaf_iid="", factory_fn=None, data=None, multi=1, _obj=None):
        if _obj: self._o = _obj; return
        
        callback = conv (factory_fn)
        if multi:
            self._o = _bonobo.bonobo_generic_factory_new_multi (oaf_iid, callback.__call__, data)
        else:
            self._o = _bonobo.bonobo_generic_factory_new (oaf_iid, callback.__call__, data)
        
    def set (self, fn, data):
        callback = conv(fn)
        _bonobo.bonobo_generic_factory_set (self._o, callback.__call__, data)
gtk._name2cls['BonoboGenericFactory'] = BonoboGenericFactory

def generic_factory_corba_object_create (object, shlib_id):
    return _bonobo.bonobo_generic_factory_corba_object_create (object, shlib_id)

# bonobo-item-container
class BonoboItemContainer (BonoboXObject):
    get_type = _bonobo.bonobo_item_container_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_item_container_new ()

    def add (self, name, object):
        _bonobo.bonobo_item_container_add (self._o, name, object._o)

    def remove_by_name (self, name):
        _bonobo.bonobo_item_container_remove_by_name (self._o, name)
gtk._name2cls['BonoboItemContainer'] = BonoboItemContainer

# bonobo-item-handler
class BonoboItemHandler (BonoboXObject):
    get_type = _bonobo.bonobo_item_handler_get_type
    def __init__ (self, enum_objects=None, get_object=None, _obj=None):
        if _obj: self._o = _obj; return
        cb1 = conv (enum_objects)
        cb2 = conv (get_object)
        self._o = _bonobo.bonobo_item_handler_new (cb1.__call__, cb2.__call__, user_data)
gtk._name2cls['BonoboItemHandler'] = BonoboItemHandler

def item_option_parse (option_string):
    return _bonobo.bonobo_item_option_parse (option_string)

#_bonobo.bonobo_item_options_free (options)

# bonobo-listener
class BonoboListener (BonoboXObject):
    get_type = _bonobo.bonobo_listener_get_type
    def __init__ (self, event_callback=None, user_data=None, _obj=None):
        if _obj: self._o = _obj; return
        callback = conv (event_callback)
        self._o = _bonobo.bonobo_listener_new (callback.__call__, user_data)
gtk._name2cls['BonoboListener'] = BonoboListener

def event_make_name (idl_path, kind, subtype):
    return _bonobo.bonobo_event_make_name (idl_path, kind, subtype)

def event_type ():
    return _bonobo.bonobo_event_type ()

def event_subtype (event_name):
    return _bonobo.bonobo_event_subtype (event_name)

def event_kind ():
    return _bonobo.bonobo_event_kind ()

def event_idl_path ():
    return _bonobo.bonobo_event_idl_path ()

# bonobo-main

def main ():
    _bonobo.bonobo_main ()

def activate ():
    return _bonobo.bonobo_activate ()

def setup_x_error_handler ():
    return _bonobo.bonobo_setup_x_error_handler ()

def orb ():
    return _bonobo.bonobo_orb ()

def poa ():
    return _bonobo.bonobo_poa ()

def poa_manager ():
    return _bonobo.bonobo_poa_manager ()

# bonobo-moniker
class BonoboMoniker (BonoboXObject):
    get_type = _bonobo.bonobo_moniker_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return

    def get_parent (self):
        return _bonobo.bonobo_moniker_get_parent (self._o)

    def set_parent (self, parent):
        _bonobo.bonobo_moniker_set_parent (self._o, parent)
        
    def get_name (self):
        return _bonobo.bonobo_moniker_get_name (self._o)
    
    def get_name_full (self):
        return _bonobo.bonobo_moniker_get_name_full (self._o)
    
    def get_name_escaped (self):
        return _bonobo.bonobo_moniker_get_name_escaped (self._o)    
    
    def set_name (self, unescaped_name, num_chars):
        _bonobo.bonobo_moniker_set_name (self._o, unescaped_name, num_chars)

    def get_prefix (self):
        return _bonobo.bonobo_moniker_get_name (self._o)

    def set_case_sensitive (self, sensitive):
        _bonobo.bonobo_moniker_set_case_sensitive (self._o, sensitive)

    def case_sensitive (self):
        return _bonobo.bonobo_moniker_get_case_sensitive (self._o)
gtk._name2cls['BonoboMoniker'] = BonoboMoniker

# bonobo-moniker-extender
#class BonoboMonikerExtender (BonoboXObject):
#    get_type = _bonobo.bonobo_moniker_extender_get_type
#    def __init__ (self, _obj=None):
#       if _obj: self._o = _obj; return
#        self._o = _bonobo.bonobo_moniker_extender_new (resolve, data)
#        
#_bonobo.bonobo_moniker_find_extender (name, interface)
#_bonobo.bonobo_moniker_use_extender (extender_oafiid, moniker, options, requested_interface)
#

# bonobo-moniker-simple
def moniker_simple_new (name, resolv_fn):
    callback = conv(resolv_fn)
    return _obj2inst (_bonobo.bonobo_moniker_simple_new (name, callback.__conv__))

# bonobo-moniker-util
def get_object (name, interface_name):
    return _bonobo.bonobo_get_object (name, interface_name)

def moniker_client_new_from_name (name):
    return _bonobo.bonobo_moniker_client_new_from_name (name)

def moniker_client_get_name (moniker):
    return _bonobo.bonobo_moniker_client_get_name (moniker)

def moniker_client_resolve_default (moniker, interface_name):
    _bonobo.bonobo_moniker_client_resolve_default (moniker, interface_name)

def moniker_client_resolve_client_default (moniker, interface_name):
    _bonobo.bonobo_moniker_client_resolve_client_default (moniker, interface_name)

def moniker_client_equal (moniker, name):
    return _bonobo.bonobo_moniker_client_equal (moniker, name)

def get_object_async (name, interface_name, timeout_msec, cb, user_data):
    callback = conv(cb)
    _bonobo.bonobo_get_object_async (name, interface_name, timeout_msec, callback.__call__, user_data)
    
def moniker_client_new_from_name_async (name, timeout_msec, cb, user_data):
    callback = conv(cb)
    _bonobo.bonobo_moniker_client_new_from_name_async (name, timeout_msec, callback.__call__, user_data)
    
def moniker_resolve_async (moniker, options, interface_name, timeout_msec, cb, user_data):
    callback = conv(cb)
    _bonobo.bonobo_moniker_resolve_async (moniker, options, interface_name, timeout_msec, callback.__call__, user_data)
    
def moniker_resolve_async_default (moniker, interface_name, timeout_msec, cb, user_data):
    callback = conv(cb)    
    _bonobo.bonobo_moniker_resolve_async_default (moniker, interface_name, timeout_msec, callback.__call__, user_data)
    
def moniker_util_new_from_name_full (parent, name):
    return _bonobo.bonobo_moniker_util_new_from_name_full (parent, name)

def moniker_util_get_parent_name (moniker):
    return _bonobo.bonobo_moniker_util_get_parent_name (moniker)

def moniker_util_qi_return (object, requested_interface):
    return _bonobo.bonobo_moniker_util_qi_return (object, requested_interface)

def moniker_util_seek_std_separator (str, min_idx):
    return _bonobo.bonobo_moniker_util_seek_std_separator (str, min_idx)

def moniker_util_escape (string, offset):
    return _bonobo.bonobo_moniker_util_escape (string, offset)

def moniker_util_unescape (string, num_chars):
    return _bonobo.bonobo_moniker_util_unescape (string, num_chars)

def url_register (oafiid, url, mime_type, object):
    _bonobo.bonobo_url_register (oafiid, url, mime_type, object)
    
def url_unregister (oafiid, url):
    _bonobo.bonobo_url_unregister (oafiid, url)
    
def url_lookup (oafiid, url):
    return _bonobo.bonobo_url_lookup (oafiid, url)

# bonobo-object-client
class BonoboObjectClient (BonoboObject):
    get_type = _bonobo.bonobo_object_client_get_type
    def __init__ (self, first=None, second=None, _obj=None):
        if _obj: self._o = _obj; return
        if type(first) == type(""):
            self._o = _bonobo.bonobo_object_activate (first, second)
        else:
            self._o = _bonobo.bonobo_object_client_from_corba (first)
        
    def has_interface (self, interface_desc):
        return _bonobo.bonobo_object_client_has_interface (self._o, interface_desc)

    def query_interface (self, interface_desc):
        return _bonobo.bonobo_object_client_query_interface (self._o, interface_desc)

    def ref (self, opt_exception_obj):
        _bonobo.bonobo_object_client_ref (self._o, opt_exception_obj)
    
    def unref (self, opt_exception_obj):
        _bonobo.bonobo_object_client_unref (self._o, opt_exception_obj)
gtk._name2cls['BonoboObjectClient'] = BonoboObjectClient

def object_activate_async (iid, oaf_flags, fn, user_data):
    callback = conv (fn)
    _bonobo.bonobo_object_activate_async (iid, oaf_flags, callback.__call__, user_data)

# bonobo-object-io
def persist_stream_save_object_iid (target, object_iid):
    _bonobo.bonobo_persist_stream_save_object_iid (target, object_iid)

def persist_stream_load_object_iid (source):
    return _bonobo.bonobo_persist_stream_load_object_iid (source)

def persiststream_save_to_stream (pstream, target, object_iid):
    return _bonobo.bonobo_persiststream_save_to_stream (pstream, target, object_iid)
        
def object_save_to_stream (object, stream, object_iid):
    return _bonobo.bonobo_object_save_to_stream (object, stream, object_iid)

# bonobo-persist

# bonobo-persist-file
class BonoboPersistFile (BonoboXObject): # (BonoboPersist):
    get_type = _bonobo.bonobo_persist_file_get_type
    def __init__ (self, load_fn=None, save_fn=None, closure=None,_obj=None):
        if _obj: self._o = _obj; return
        cb_load = conv (load_fn)
        cb_save = conv (save_fn)
        self._o = _bonobo.bonobo_persist_file_new (cb_load.__call__, cb_save.__call__, closure)
gtk._name2cls['BonoboPersistFile'] = BonoboPersistFile

# bonobo-persist-stream
class BonoboPersistStream (BonoboXObject): # (BonoboPersist):
    get_type = _bonobo.bonobo_persist_stream_get_type
    def __init__ (self, load_fn=None, save_fn=None, max_fn=None, types_fn=None, _obj=None):
        if _obj: self._o = _obj; return
        load_cb = conv (load_fn)
        save_fb = conv (save_fn)
        max_cb = conv (max_fn)
        types_cb = conv (types_fn)
        self._o = _bonobo.bonobo_persist_stream_new (load_cb.__call__,
                                                     save_cb.__call__,
                                                     max_cb.__call__,
                                                     types_cb.__call__, closure)

    def set_dirty (self, dirty):
        return _bonobo.bonobo_persist_stream_set_dirty (self._o, dirty)
gtk._name2cls['BonoboPersistStream'] = BonoboPersistStream

# bonobo-plug
class BonoboPlug (gtk.GtkWindow):
    get_type = _bonobo.bonobo_plug_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_plug_new (socket_id)

    def set_control (self, control):
        _bonobo.bonobo_plug_set_control (self._o, control._o)

    def clear_focus_chain (self):
        _bonobo.bonobo_plug_clear_focus_chain (self._o)
gtk._name2cls['BonoboPlug'] = BonoboPlug

# bonobo-print-client
class BonoboPrintClient (gtk.GtkObject):
    get_type = _bonobo.bonobo_print_client_get_type
    def __init__ (self, corba_print=None, _obj=None):
        if _obj: self._o = _obj; return

        self._o = _bonobo.bonobo_print_client_new (corba_print)

    def render (self, pd):
        return _bonobo.bonobo_print_client_render (self._o, pd._o)
gtk._name2cls['BonoboPrintClient'] = BonoboPrintClient

# This is a total mess.
# Fixelifixfix
class BonoboPrintData:
    width = -1
    height = -1
    width_first_page = -1
    width_per_page = -1
    height_first_page = -1
    height_per_page = -1
    
def print_data_new (width, height):
    return _obj2inst (_bonobo.bonobo_print_data_new (width, height))

#def print_data_new_full (...):
#    return _bonobo.bonobo_print_data_new_full (width, height, width_first_page, width_per_page, height_first_page, height_per_page)

def get_meta (self):
    return _bonobo.bonobo_print_data_get_meta (self._o)
    
def print_data_render (pc, x, y, pd, meta_x, meta_y):
    _bonobo.bonobo_print_data_render (pc, x, y, pd, meta_x, meta_y)
    
def print_client_get (object):
    return _obj2inst ( _bonobo.bonobo_print_client_get (object))

# bonobo-print
class BonoboPrint (BonoboXObject):
    get_type = _bonobo.bonobo_print_get_type
    def __init__ (self, render=None,_obj=None):
        if _obj: self._o = _obj; return
        callback = conv (render)
        self._o = _bonobo.bonobo_print_new (callback.__call__, user_data)
gtk._name2cls['BonoboPrint'] = BonoboPrint

# bonobo-progressive
# FIXME
#class BonoboProgressiveDataSink:
#    get_type = _bonobo.bonobo_progressive_data_sink_get_type
#    def __init__ (self, _obj=None):
#        if _obj: self._o = _obj; return
#        self._o = _bonobo.bonobo_progressive_data_sink_new (start_fn, end_fn, add_data_fn, set_size_fn, closure)

# bonobo-property-bag-client
def property_bag_client_get_properties (pb):
    return _bonobo.bonobo_property_bag_client_get_properties (pb)

def property_bag_client_get_property_names (pb):
    return _bonobo.bonobo_property_bag_client_get_property_names (pb)

def property_bag_client_get_property (pb, property_name):
    return _bonobo.bonobo_property_bag_client_get_property (pb, property_name)

def property_bag_client_persist (pb, stream):
    _bonobo.bonobo_property_bag_client_persist (pb, stream)
    
def property_bag_client_depersist (pb, stream):
    _bonobo.bonobo_property_bag_client_depersist (pb, stream)

#def property_bag_client_setv (pb, first_arg, var_args):
#    return _bonobo.bonobo_property_bag_client_setv (pb, first_arg, var_args)

#def property_bag_client_getv (pb, first_arg, var_args):
#    return _bonobo.bonobo_property_bag_client_getv (pb, first_arg, var_args)

def property_bag_client_get_property_type (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_property_type (pb, propname)

def property_bag_client_get_value_gboolean (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_value_gboolean (pb, propname)

def property_bag_client_get_value_gint (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_value_gint (pb, propname)

def property_bag_client_get_value_glong (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_value_glong (pb, propname)
    
def property_bag_client_get_value_gfloat (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_value_gfloat (pb, propname)

def property_bag_client_get_value_gdouble (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_value_gdouble (pb, propname)

def property_bag_client_get_value_string (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_value_string (pb, propname)

def property_bag_client_get_value_any (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_value_any (pb, propname)

def property_bag_client_get_default_gboolean (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_default_gboolean (pb, propname)

def property_bag_client_get_default_gint (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_default_gint (pb, propname)

def property_bag_client_get_default_glong (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_default_glong (pb, propname)

def property_bag_client_get_default_gfloat (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_default_gfloat (pb, propname)

def property_bag_client_get_default_gdouble (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_default_gdouble (pb, propname)

def property_bag_client_get_default_string (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_default_string (pb, propname)
    
def property_bag_client_get_default_any (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_default_any (pb, propname)

def property_bag_client_set_value_gboolean (pb, propname, value):
    _bonobo.bonobo_property_bag_client_set_value_gboolean (pb, propname, value)

def property_bag_client_set_value_gint (pb, propname, value):
    _bonobo.bonobo_property_bag_client_set_value_gint (pb, propname, value)

def property_bag_client_set_value_glong (pb, propname, value):
    _bonobo.bonobo_property_bag_client_set_value_glong (pb, propname, value)

def property_bag_client_set_value_gfloat (pb, propname, value):
    _bonobo.bonobo_property_bag_client_set_value_gfloat (pb, propname, value)

def property_bag_client_set_value_gdouble (pb, propname, value):
    _bonobo.bonobo_property_bag_client_set_value_gdouble (pb, propname, value)
    
def property_bag_client_set_value_string (pb, propname, value):
    _bonobo.bonobo_property_bag_client_set_value_string (pb, propname, value)

def property_bag_client_set_value_any (pb, propname, value):
    _bonobo.bonobo_property_bag_client_set_value_any (pb, propname, value)

def property_bag_client_get_docstring (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_docstring (pb, propname)

def property_bag_client_get_flags (pb, propname):
    return _bonobo.bonobo_property_bag_client_get_flags (pb, propname)


# bonobo-property-bag
class BonoboPropertyBag (BonoboXObject):
    get_type = _bonobo.bonobo_property_bag_get_type
    def __init__ (self, get_prop=None, set_prop=None, event_source=None, user_data=None, full=0, _obj=None):
        if _obj: self._o = _obj; return
        
        if get_prop: get_prop = conv (get_prop).__call__
        if set_prop: set_prop = conv (set_prop).__call__
        
        if not full:
            self._o = _bonobo.bonobo_property_bag_new (get_prop, set_prop, user_data)
        else:
            self._o = _bonobo.bonobo_property_bag_new_full (get_prop, set_prop, user_data)

    def add (self, name, idx, type, default_value=None, docstring=None, flags=BONOBO.PROPERTY_READABLE):
        _bonobo.bonobo_property_bag_add (self._o, name, idx, type, default_value, docstring, flags)

    def add_full (self, name, idx, type, default_value, docstring, flags, get_prop, set_prop, user_data):
        return _bonobo.bonobo_property_bag_add_full (self._o, name, idx, type, default_value, docstring, flags, get_prop, set_prop, user_data)

    def add_gtk_args (self, object):
        return _bonobo.bonobo_property_bag_add_gtk_args (self._o, object)

    def get_property_type (self, name):
        return _bonobo.bonobo_property_bag_get_property_type (self._o, name)

    def set_value (self, name, value):
        _bonobo.bonobo_property_bag_set_value (self._o, name, value)

    def get_value (self, name):
        return _bonobo.bonobo_property_bag_get_value (self._o, name)

    def get_default (self, name):
        return _bonobo.bonobo_property_bag_get_default (self._o, name)

    def get_docstring (self, name):
        return _bonobo.bonobo_property_bag_get_docstring (self._o, name)
    
    def has_property (self, name):
        return _bonobo.bonobo_property_bag_has_property (self._o, name)

    def notify_listeners (self, name, new_value):
        return _bonobo.bonobo_property_bag_notify_listeners (self._o, name, new_value)

    def get_prop_list (self):
        return _bonobo.bonobo_property_bag_get_prop_list (self._o)
gtk._name2cls['BonoboPropertyBag'] = BonoboPropertyBag

# bonobo-property-bag-xml
def property_bag_xml_encode_any (opt_parent, any):
    return BonoboUINode (_obj=_bonobo.bonobo_property_bag_xml_encode_any (opt_parent, any))

def bonobo_property_bag_xml_decode_any (node):
    return _bonobo.bonobo_property_bag_xml_decode_any (node)

# bonobo-property-control
class BonoboPropertyControl (BonoboXObject):
    get_type = _bonobo.bonobo_property_control_get_type
    def __init__ (self, get_fn=None, num_pages=0, closure=None, event_source=None, full=0, _obj=None):
        if _obj: self._o = _obj; return
        callback = conv(get_fn)
        if not full:
            self._o = _bonobo.bonobo_property_control_new (callback.__call__,  num_pages, closure)
        else:
            self._o = _bonobo.bonobo_property_control_new_full (callback.__call__, num_pages, event_source._o, closure)

    def changed (self):
        _bonobo.bonobo_property_control_changed (self._o)

    def get_event_source (self):
        return _obj2inst (_bonobo.bonobo_property_control_get_event_source (self._o))
gtk._name2cls['BonoboPropertyControl'] = BonoboPropertyControl

# bonobo-property
def bonobo_property_servant_new (poa, bt, name, callback_data):
    return _bonobo.bonobo_property_servant_new (poa, bt, name, callback_data)

#_bonobo.bonobo_property_servant_destroy (servant, callback_data)

# bonobo-selector
class BonoboSelector (gnome.ui.GnomeDialog):
    get_type = _bonobo.bonobo_selector_get_type
    def __init__ (self, title="", interfaces_required=None, _obj=None):
       if _obj: self._o = _obj; return
       self._o = _bonobo.bonobo_selector_new (title, interfaces_required)

    def selector_get_selected_name (self):
        return _bonobo.bonobo_selector_get_selected_name (self._o)

    def get_selected_description (self):
        return _bonobo.bonobo_selector_get_selected_description (self._o)

    def get_selected_id (self):
        return _bonobo.bonobo_selector_get_selected_id (self._o)

    def selected_id (self, title, interfaces_required):
        return _bonobo.bonobo_selector_selected_id (self._o, title, interfaces_required)
gtk._name2cls['BonoboSelector'] = BonoboSelector

# bonobo-selector-widget
class BonoboSelectorWidget (gtk.GtkVBox):
    get_type = _bonobo.bonobo_selector_widget_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_selector_widget_new ()

    def set_interface (self, interfaces_required):
        _bonobo.bonobo_selector_widget_set_interface (self._o, interfaces_required)
        
    def get_description (self):
        return _bonobo.bonobo_selector_widget_get_description (self._o)

    def get_name (self):
        return _bonobo.bonobo_selector_widget_get_name (self._o)

    def get_id (self):
        return _bonobo.bonobo_selector_widget_get_id (self._o)
gtk._name2cls['BonoboSelectorWidget'] = BonoboSelectorWidget

# bonobo-socket
class BonoboSocket (gtk.GtkContainer):
    get_type = _bonobo.bonobo_socket_get_type ()
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_socket_new ()

    def set_control_frame (self, frame):
        _bonobo.bonobo_socket_set_control_frame (self._o, frame._o)

    def steal (self, wid):
        _bonobo.bonobo_socket_steal (self._o, wid)
gtk._name2cls['BonoboSocket'] = BonoboSocket

# bonobo-storage
class BonoboStorage (BonoboXObject):
    get_type = _bonobo.bonobo_storage_get_type
    def __init__ (self, driver="", path="", flags=0, mode=0, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_storage_open_full (driver, path, flags, mode)
        
    def write_class_id (self, class_id):
        _bonobo.bonobo_storage_write_class_id (self._o, class_id)
gtk._name2cls['BonoboStorage'] = BonoboStorage

def storage_copy_to (src, dest):
    _bonobo.bonobo_storage_copy_to (src, dest)
    
def stream_write_class_id (stream, class_id):
    _bonobo.bonobo_stream_write_class_id (stream._o, class_id)

# bonobo-storage-plugin
#_bonobo.bonobo_storage_plugin_find (name)
#_bonobo.bonobo_storage_load_plugins ()

# bonobo-stream-client
#_bonobo.bonobo_stream_client_get_length (stream)
#_bonobo.bonobo_stream_client_printf (stream, terminate, fmt)
#_bonobo.bonobo_stream_client_read (stream, size, length_read)
#_bonobo.bonobo_stream_client_write_string (stream, str, terminate)

# bonobo-stream
class BonoboStream (BonoboObject):
    get_type = _bonobo.bonobo_stream_get_type
    def __init__ (self, driver="", path="", flags=0, mode=0, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_stream_open (driver, path, flags, mode)
#        self._o = _bonobo.bonobo_stream_corba_object_create (object)
#        self._o = _bonobo.bonobo_stream_open_full (driver, path, flags, mode)
gtk._name2cls['BonoboStream'] = BonoboStream

# bonobo-stream-memory
class BonoboStreamMem (BonoboStream):
    get_type = _bonobo.bonobo_stream_mem_get_type
    def __init__ (self, _obj=None):
	if _obj: self._o = _obj; return

    def bonobo_stream_mem_get_size (self):
        return _bonobo.bonobo_stream_mem_get_size (self._o)
gtk._name2cls['BonoboStreamMem'] = BonoboStreamMem

def stream_mem_create (size, read_only, resizeable):
    return BonoboStreamMem (_obj=_bonobo.bonobo_stream_mem_create (size, read_only, resizeable))

# bonobo-transient
class BonoboTransient (gtk.GtkObject):
    get_type = _bonobo.bonobo_transient_get_type
    def __init__ (self, new_servant=None, destory_servant=None, _obj=None):
        if _obj: self._o = _obj; return
        cb1 = conv (new_servant)
        cb2 = conv (destroy_servant)
        self._o = _bonobo.bonobo_transient_new (poa, cb1.__call__, cb2.__call__, data)

    def create_objref (self, iface_name, name):
        return _bonobo.bonobo_transient_create_objref (self._o, iface_name, name)
gtk._name2cls['BonoboTransient'] = BonoboTransient

# bonobo-ui-component
class BonoboUIComponent (BonoboXObject):
    get_type = _bonobo.bonobo_ui_component_get_type
    def __init__ (self, name="", _obj=None):
        if _obj: self._o = _obj; return
        if name:
            self._o = _bonobo.bonobo_ui_component_new (name)
        else:
            self._o = _bonobo.bonobo_ui_component_new_default ()

    def set_name (self, name):
        _bonobo.bonobo_ui_component_set_name (self._o, name)

    def get_name (self, name):
        return _bonobo.bonobo_ui_component_get_name (self._o)

    def object_get (self, path):
        return _bonobo.bonobo_ui_component_object_get (self._o, path)

    def set_container (self, container):
        _bonobo.bonobo_ui_component_set_container (self._o, container)

    def unset_container (self):
        return _bonobo.bonobo_ui_component_unset_container (self._o)

    def get_container (self):
        return _bonobo.bonobo_ui_component_get_container (self._o)

    def add_verb (self, cname, fn, user_data=None):
        callback = conv (fn)
        _bonobo.bonobo_ui_component_add_verb (self._o, cname, callback.__call__, user_data)

    def add_verb_full (self, cname, fn, user_data=None, destroy_fn=None):
        cb1 = conv (fn)
        cb2 = conv (destory_fn)
        _bonobo.bonobo_ui_component_add_verb_full (self._o, cname, cb1.__call__, user_data, cb2.__call__)

    def remove_verb (self, cname):
        _bonobo.bonobo_ui_component_remove_verb (self._o, cname)

    def remove_verb_by_func (self, fn):
        # TODO: Is this necessary?
        callback = conv (fn)
        _bonobo.bonobo_ui_component_remove_verb_by_func (self._o, callback.__call__)

    def remove_verb_by_data (self, user_data):
        _bonobo.bonobo_ui_component_remove_verb_by_data (self._o, user_data)

    def add_listener (self, id, fn, user_data=None):
        callback = conv (fn)
        _bonobo.bonobo_ui_component_add_listener (self._o, id, callback.__call__, user_data)

    def add_listener_full (self, id, fn, user_data, destroy_fn):
        cb1 = conv (fn)
        cb2 = conv (destroy_fn)
        _bonobo.bonobo_ui_component_add_listener_full (self._o, id, cb1.__call__, user_data, cb2.__call__)

    def remove_listener (self, cname):
        _bonobo.bonobo_ui_component_remove_listener (self._o, cname)

    def remove_listener_by_func (self, fn):
        _bonobo.bonobo_ui_component_remove_listener_by_func (self._o, fn)

    def remove_listener_by_data (self, user_data):
        _bonobo.bonobo_ui_component_remove_listener_by_data (self._o, user_data)

    def set (self, path, xml):
        _bonobo.bonobo_ui_component_set (self._o, path, xml)

    def set_translate (self, path, xml):
        _bonobo.bonobo_ui_component_set_translate (self._o, path, xml)

    def set_tree (self, path, node):
        _bonobo.bonobo_ui_component_set_tree (self._o, path, node._o)

    def rm (self, path):
        _bonobo.bonobo_ui_component_rm (self._o, path)

    def path_exists (self, path):
        return _bonobo.bonobo_ui_component_path_exists (self._o, path)

    def get (self, path, recurse):
        return _bonobo.bonobo_ui_component_get (self._o, path, recurse)

    def get_tree (self, path, recurse):
        return BonoboUINode (_obj=_bonobo.bonobo_ui_component_get_tree (self._o, path, recurse))

    def object_set (self, path, control):
        _bonobo.bonobo_ui_component_object_set (self._o, path, control)

    def object_get (self, path):
        return _bonobo.bonobo_ui_component_object_set (self._o, path)
    
    def freeze (self):
        _bonobo.bonobo_ui_component_freeze (self._o)

    def thaw (self):
        _bonobo.bonobo_ui_component_thaw (self._o)

    def set_prop (self, path, prop, value):
        _bonobo.bonobo_ui_component_set_prop (self._o, path, prop, value)

    def get_prop (self, path, prop):
        return _bonobo.bonobo_ui_component_get_prop (self._o, path, prop)

    def set_status (self, text):
        _bonobo.bonobo_ui_component_set_status (self._o, text)

    def add_verb_list (self, dict):
        for verb in dict.keys():
            self.add_verb (verb, dict[verb])

    def add_verb_list_with_data (self, dict, data):
        for verb in dict.keys():
            self.add_verb (verb, dict[verb], data)
gtk._name2cls['BonoboUIComponent'] = BonoboUIComponent

# bonobo-ui-config-widget
class BonoboUIConfigWidget (gtk.GtkVBox):
    get_type = _bonobo.bonobo_ui_config_widget_get_type
    def __init__ (self, engine=None, accel_group=None, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_ui_config_widget_new (engine, accel_group)
gtk._name2cls['BonoboUIConfigWidget'] = BonoboUIConfigWidget

# bonobo-ui-container
class BonoboUIContainer (BonoboXObject):
    get_type = _bonobo.bonobo_ui_container_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_ui_container_new ()

    def set_engine (self, engine):
        _bonobo.bonobo_ui_container_set_engine (self._o, engine._o)

    def get_engine (self):
        return _obj2inst (_bonobo.bonobo_ui_container_get_engine (self._o))

    def set_win (self, win):
        _bonobo.bonobo_ui_container_set_win (self._o, win._o)

    def get_win (self):
        return _obj2inst (_bonobo.bonobo_ui_container_get_win (self._o))
gtk._name2cls['BonoboUIContainer'] = BonoboUIContainer

# bonobo-ui-engine
class BonoboUIEngine (gtk.GtkObject):
    get_type = _bonobo.bonobo_ui_engine_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_ui_engine_new ()

    def config_set_path (self, path):
        _bonobo.bonobo_ui_engine_config_set_path (self._o, path)
    
    def config_get_path (self, path):
        return _bonobo.bonobo_ui_engine_config_set_path (self._o, path)
    
    def add_sync (self, sync):
        _bonobo.bonobo_ui_engine_add_sync (self._o, sync._o)

    def remove_sync (self, sync):
        _bonobo.bonobo_ui_engine_remove_sync (self._o, sync._o)

    def get_syncs (self):
        return _bonobo.bonobo_ui_engine_get_syncs (self._o)

    def update (self):
        _bonobo.bonobo_ui_engine_update (self._o)

    def update_node (self, node):
        _bonobo.bonobo_ui_engine_update_node (self._o, node._o)

    def queue_update (self, widget, node, cmd_node):
        _bonobo.bonobo_ui_engine_queue_update (self._o, widget._o, node._o, cmd_node._o)

    def build_control (self, node):
        return _obj2inst (_bonobo.bonobo_ui_engine_build_control (self._o, node._o))

    def widget_set_node (self, widget, node):
        _bonobo.bonobo_ui_engine_widget_set_node (self._o, widget._o, node._o)

    def prune_widget_info (self, node, save_custom):
        _bonobo.bonobo_ui_engine_prune_widget_info (self._o, node._o, save_custom)

    def get_path (self, path):
        return BonoboUINode (_obj=_bonobo.bonobo_ui_engine_get_path (self._o, path._o))

    def dirty_tree (self, node):
        _bonobo.bonobo_ui_engine_dirty_tree (self._o, node._o)

    def clean_tree (self, node):
        _bonobo.bonobo_ui_engine_clean_tree (self._o, node._o)

    # def dump (self, out, msg)
    
    def node_get_object (self, node):
        return _bonobo.bonobo_ui_engine_node_get_object (self._o, node._o)

    def node_is_dirty (self, node):
        return _bonobo.bonobo_ui_engine_node_is_dirty (self._o, node._o)

    def node_get_widget (self, node):
        return _obj2inst (_bonobo.bonobo_ui_engine_node_get_widget (self._o, node))

    # def get_id (self, node)

    def get_cmd_node (self, from_node):
        return BonoboUINode (_obj=_bonobo.bonobo_ui_engine_get_cmd_node (self._o, from_node._o))
    
    def node_set_dirty (self, node, dirty):
        _bonobo.bonobo_ui_engine_node_set_dirty (self._o, node._o, dirty)

    def stamp_custom (self, node):
        _bonobo.bonobo_ui_engine_stamp_custom (self._o, node._o)

    def stamp_root (self, node, widget):
        _bonobo.bonobo_ui_engine_stamp_root (self._o, node._o, widget._o)

    def add_hint (self, str):
        _bonobo.bonobo_ui_engine_add_hint (self._o, str)

    def remove_hint (self):
        _bonobo.bonobo_ui_engine_remove_hint (self._o)

    def emit_verb_on (self, node):
        _bonobo.bonobo_ui_engine_emit_verb_on (self._o, node._o)

    def emit_event_on (self, node, state):
        _bonobo.bonobo_ui_engine_emit_event_on (self._o, node._o, state)

    def emit_verb_on_w (self, widget):
        _bonobo.bonobo_ui_engine_emit_verb_on_w (self._o, widget._o)

    def emit_event_on_w (self, widget, state):
        _bonobo.bonobo_ui_engine_emit_event_on_w (self._o, widget._o, state)

    def deregister_dead_components (self):
        _bonobo.bonobo_ui_engine_deregister_dead_components (self._o)

    def deregister_component_by_ref (self, ref):
        _bonobo.bonobo_ui_engine_deregister_component_by_ref (self._o, ref)

    def deregister_component (self, name):
        _bonobo.bonobo_ui_engine_deregister_component (self._o, name)

    def register_component (self, name, component):
        _bonobo.bonobo_ui_engine_register_component (self._o, name, component)

    def get_component (self, name):
        return _bonobo.bonobo_ui_engine_get_component (self._o, name)

    # def get_component_names (self)
    
    def xml_get (self, path, node_only):
        return _bonobo.bonobo_ui_engine_xml_get (self._o, path, node_only)

    def xml_node_exists (self, path):
        return _bonobo.bonobo_ui_engine_xml_node_exists (self._o, path)

    def xml_merge_tree (self, path, tree, component):
        return _bonobo.bonobo_ui_engine_xml_merge_tree (self._o, path, tree._o, component)

    def xml_rm (self, path, by_component):
        return _bonobo.bonobo_ui_engine_xml_rm (self._o, path, by_component)

    def object_set (self, path, object):
        return _bonobo.bonobo_ui_engine_object_set (self._o, path, object)

    # def object_get (self, path, object)
    
    def set_ui_container (self, ui_container):
        return _bonobo.bonobo_ui_engine_set_ui_container (self._o, ui_container._o)

    def freeze (self):
        return _bonobo.bonobo_ui_engine_freeze (self._o)

    def thaw (self):
        return _bonobo.bonobo_ui_engine_thaw (self._o)
gtk._name2cls['BonoboUIEngine'] = BonoboUIEngine

#def widget_get_node (self, widget)
#   self._o = _bonobo.bonobo_ui_engine_widget_get_node (widget)
#def get_attr (self, cmd_name, attr)
#   self._o = _bonobo.bonobo_ui_engine_get_attr (node, cmd_node, attr)
#def widget_attach_node (self, node)
#   self._o = _bonobo.bonobo_ui_engine_widget_attach_node (widget, node)
    
# bonobo-ui-node
class BonoboUINode:
    def __init__ (self, name="", _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_ui_node_new (name)
        
    def new_child (self, name):
        return BonoboUINode (_obj=_bonobo.bonobo_ui_node_new_child (self._o, name))

    def copy (self, recursive):
        return BonoboUINode (_obj=_bonobo.bonobo_ui_node_copy (self._o, recursive))

    def free (self):
        _bonobo.bonobo_ui_node_free (self._o)

    def set_data (self, data):
        _bonobo.bonobo_ui_node_set_data (self._o, data)

    def get_data (self):
        return _bonobo.bonobo_ui_node_get_data (self._o)

    def set_attr (self, name, value):
        _bonobo.bonobo_ui_node_set_attr (self._o, name, value)

    def get_attr (self, name):
        return _bonobo.bonobo_ui_node_get_attr (self._o, name)

    def has_attr (self, name):
        return _bonobo.bonobo_ui_node_has_attr (self._o, name)

    def remove_attr (self, name):
        _bonobo.bonobo_ui_node_remove_attr (self._o, name)

    def add_child (self, child):
        _bonobo.bonobo_ui_node_add_child (self._o, child._o)

    # Check .c (args)
    def insert_before (self, prev_sibling):
        _bonobo.bonobo_ui_node_insert_before (self._o, prev_sibling._o)

    def unlink (self):
        _bonobo.bonobo_ui_node_unlink (self._o)

    def replace (self, new_node):
        _bonobo.bonobo_ui_node_replace (self._o, new_node._o)

    def set_content (self, content):
        _bonobo.bonobo_ui_node_set_content (self._o, content)

    def get_content (self):
        return _bonobo.bonobo_ui_node_get_content (self._o)

    def next (self):
        return BonoboUINode (_obj=_bonobo.bonobo_ui_node_next (self._o))

    def prev (self):
        return BonoboUINode (_obj=_bonobo.bonobo_ui_node_prev (self._o))

    def children (self):
        return BonoboUINode (_obj=_bonobo.bonobo_ui_node_children (self._o))

    def parent (self):
        return BonoboUINode (_obj=_bonobo.bonobo_ui_node_parent (self._o))
    
    # def get_name (self)
    
    def has_name (self, name):
        return _bonobo.bonobo_ui_node_has_name (self._o, name)

    def transparent (self):
        return _bonobo.bonobo_ui_node_transparent (self._o)
    
    def copy_attrs (self, dest):
         _bonobo.bonobo_ui_node_copy_attrs (self._o, dest._o)

    def to_string (self, recurse):
        return _bonobo.bonobo_ui_node_to_string (self._o, recurse)
gtk._name2cls['BonoboUINode'] = BonoboUINode

#_bonobo.bonobo_ui_node_from_string (str)
#_bonobo.bonobo_ui_node_from_file (filename)

# bonobo-ui-sync
class BonoboUISync (gtk.GtkObject):
    get_type = _bonobo.bonobo_ui_sync_status_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return

    def is_recursive (self):
        return _bonobo.bonobo_ui_sync_is_recursive (self._o)

    def has_widgets (self):
        return _bonobo.bonobo_ui_sync_has_widgets (self._o)

    def remove_root (self, root):
        _bonobo.bonobo_ui_sync_remove_root (self._o, root._o)

    def update_root (self, root):
        _bonobo.bonobo_ui_sync_update_root (self._o, root._o)

    def state (self, node, cmd_node, widget, parent):
        _bonobo.bonobo_ui_sync_state (self._o, node._o, cmd_node._o, widget._o, parent._o)

    def state_placeholder (self, node, cmd_node, widget, parent):
        _bonobo.bonobo_ui_sync_state_placeholder (self._o, node._o, cmd_node._o, widget._o, parent._o)

    def build (self, node, cmd_node, pos, parent):
        return _obj2inst (_bonobo.bonobo_ui_sync_build (self._o, node._o, cmd_node._o, pos, parent._o))

    def build_placeholder (self, node, cmd_node, pos, parent):
        return _obj2inst (_bonobo.bonobo_ui_sync_build_placeholder (self._o, node._o, cmd_node._o, pos, parent._o))

    def ignore_widget (self, widget):
        return _bonobo.bonobo_ui_sync_ignore_widget (self._o, widget._o)

    # def get_widgets (self, node):
    
    def stamp_root (self):
        _bonobo.bonobo_ui_sync_stamp_root (self._o)

    def can_handle (self, node):
        return _bonobo.bonobo_ui_sync_can_handle (self._o, node._o)

    def get_attached (self, widget, node):
        return _obj2inst (_bonobo.bonobo_ui_sync_get_attached (self._o, widget._o, node._o))

    def state_update (self, widget, new_state):
        _bonobo.bonobo_ui_sync_state_update (self._o, widget._o, new_state)

    def do_show_hide (self, node, cmd_node, widget):
        return _bonobo.bonobo_ui_sync_do_show_hide (self._o, node._o, cmd_node._o, widget._o)
gtk._name2cls['BonoboUISync'] = BonoboUISync

# bonobo-ui-toolbar-item
class BonoboUIToolbarItem (gtk.GtkBin):
    get_type = _bonobo.bonobo_ui_toolbar_item_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_ui_toolbar_item_new ()

    def set_tooltip (self, tooltips, tooltip):
        _bonobo.bonobo_ui_toolbar_item_set_tooltip (self._o, tooltips, tooltip)

    def set_state (self, new_state):
        _bonobo.bonobo_ui_toolbar_item_set_state (self._o, new_state)
    
    def set_orientation (self, orientation):
        _bonobo.bonobo_ui_toolbar_item_set_orientation (self._o, orientation)

    def get_orientation (self):
        return _bonobo.bonobo_ui_toolbar_item_get_orientation (self._o)

    def set_style (self, style):
        _bonobo.bonobo_ui_toolbar_item_set_style (self._o, style)

    def get_style (self):
        return _bonobo.bonobo_ui_toolbar_item_get_style (self._o)

    def set_minimum_width (self, minimum_width):
        _bonobo.bonobo_ui_toolbar_item_set_minimum_width (self._o, minimum_width)

    def set_want_label (self, prefer_text):
        _bonobo.bonobo_ui_toolbar_item_set_want_label (self._o, prefer_text)

    def get_want_label (self):
        return _bonobo.bonobo_ui_toolbar_item_get_want_label (self._o)

    def set_expandable (self, expandable):
        _bonobo.bonobo_ui_toolbar_item_set_expandable (self._o, expandable)

    def get_expandable (self):
        return _bonobo.bonobo_ui_toolbar_item_get_expandable (self._o)

    def set_pack_end (self, expandable):
        _bonobo.bonobo_ui_toolbar_item_set_pack_end (self._o, expandable)

    def get_pack_end (self):
        return _bonobo.bonobo_ui_toolbar_item_get_pack_end (self._o)

    def item_activate (self):
        _bonobo.bonobo_ui_toolbar_item_activate (self._o)
gtk._name2cls['BonoboUIToolbarItem'] = BonoboUIToolbarItem

# bonobo-ui-toolbar-button-item
class BonoboUIToolbarButtonItem (BonoboUIToolbarItem):
    get_type = _bonobo.bonobo_ui_toolbar_button_item_get_type
    def __init__ (self, icon=None, label="", _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_ui_toolbar_button_item_new (icon, label)

    def set_icon (self, icon):
        _bonobo.bonobo_ui_toolbar_button_item_set_icon (self._o, icon)

    def set_label (self, label):
        _bonobo.bonobo_ui_toolbar_button_item_set_label (self._o, label)

    def get_button_widget (self):
        return _obj2inst (_bonobo.bonobo_ui_toolbar_button_item_get_button_widget (self._o))
gtk._name2cls['BonoboUIToolbarButtonItem'] = BonoboUIToolbarButtonItem

# bonobo-ui-toolbar-icon
class BonoboUIToolbarIcon (gtk.GtkMisc):
    get_type = _bonobo.bonobo_ui_toolbar_icon_get_type ()
    def __init__ (self, arg=None, width=0, height=0, _obj=None):
        if _obj: self._o = _obj; return
        
        if type (arg) == type (""):
            if width and height:
                self._o = _bonobo.bonobo_ui_toolbar_icon_new_from_file_at_size (arg, width, height)
            else:
                self._o = _bonobo.bonobo_ui_toolbar_icon_new_from_file (arg)
        # FIXME: type(arg) == type(GdkPixbufType)
        elif arg:
            if width and height:
                self._o = _bonobo.bonobo_ui_toolbar_icon_new_from_pixbuf_at_size (arg, width, height)
            else:
                self._o = _bonobo.bonobo_ui_toolbar_icon_new_from_pixbuf (arg)
        elif not arg:
            self._o = _bonobo.bonobo_ui_toolbar_icon_new ()
        # FIXME: else print error
            
    def set_pixbuf_size (self, width, height):
        _bonobo.bonobo_ui_toolbar_icon_set_pixbuf_size (self._o, width, height)

    # TODO: move from auto to manual
    #def get_pixbuf_size (self, width, height):
    #    return _bonobo.bonobo_ui_toolbar_icon_get_pixbuf_size (self._o, width, height)

    def set_pixbuf (self, pixbuf):
        _bonobo.bonobo_ui_toolbar_icon_set_pixbuf (self._o, pixbuf)
    
    def get_pixbuf (self):
        return _obj2inst (_bonobo.bonobo_ui_toolbar_icon_get_pixbuf (self._o))

    def set_pixbuf_at_state (self, state, pixbuf, mask):
        return _bonobo.bonobo_ui_toolbar_icon_set_pixbuf_at_state (self._o, state, pixbuf, mask)

    # def set_state_pixbufs (self, pixbufs, masks)

    def set_draw_vals (self, state, saturation, pixelate):
        _bonobo.bonobo_ui_toolbar_icon_set_draw_vals (self._o, state, saturation, pixelate)

    # TODO: move from auto to manual
    def get_draw_vals (self, state, saturation, pixelate):
        _bonobo.bonobo_ui_toolbar_icon_get_draw_vals (self._o, state, saturation, pixelate)

    def set_draw_mode (self, mode):
        _bonobo.bonobo_ui_toolbar_icon_set_draw_mode (self._o, mode)

    def get_draw_mode (self):
        return _bonobo.bonobo_ui_toolbar_icon_get_draw_mode (self._o)

    def clear (self):
        _bonobo.bonobo_ui_toolbar_icon_clear (self._o)

    def set_alpha_threshold (self, alpha_threshold):
        _bonobo.bonobo_ui_toolbar_icon_set_alpha_threshold (self._o, alpha_threshold)
    
    def get_alpha_threshold (self):
        return _bonobo.bonobo_ui_toolbar_icon_get_alpha_threshold (self._o)
gtk._name2cls['BonoboUIToolbarIcon'] = BonoboUIToolbarIcon

# bonobo-ui-toolbar-toggle-button-item
class BonoboUIToolbarToggleButtonItem (BonoboUIToolbarButtonItem):
    get_type = _bonobo.bonobo_ui_toolbar_toggle_button_item_get_type
    def __init__ (self, icon=None, label="", _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_ui_toolbar_toggle_button_item_new (icon, label)

    def set_active (self, active):
        _bonobo.bonobo_ui_toolbar_toggle_button_item_set_active (self._o, active)

    def get_active (self):
        return _bonobo.bonobo_ui_toolbar_toggle_button_item_get_active (self._o)
gtk._name2cls['BonoboUIToolbarToggleButtonItem'] = BonoboUIToolbarToggleButtonItem

# bonobo-ui-util
def ui_util_pixbuf_to_xml (pixbuf):
    return _bonobo.bonobo_ui_util_pixbuf_to_xml (pixbuf)

def ui_util_xml_to_pixbuf (xml):
    return _obj2inst (_bonobo.bonobo_ui_util_xml_to_pixbuf (xml))

def ui_util_xml_get_icon_pixbuf (node, prepend_menu):
    return _obj2inst (_bonobo.bonobo_ui_util_xml_get_icon_pixbuf (node._o, prepend_menu))

def ui_util_xml_get_icon_pixmap_widget (node, prepend_menu):
    return _obj2inst (_bonobo.bonobo_ui_util_xml_get_icon_pixmap_widget (node._o, prepend_menu))
    
def ui_util_xml_set_pixbuf (node, pixbuf):
    _bonobo.bonobo_ui_util_xml_set_pixbuf (node._o, pixbuf._o)
    
# def ui_util_set_pix_xpm (node, xpm)

def ui_util_xml_set_pix_stock (node, name):
    _bonobo.bonobo_ui_util_xml_set_pix_stock (node._o, name)

def ui_util_xml_set_pix_fname (node, name):
    _bonobo.bonobo_ui_util_xml_set_pix_fname (node._o, name)
    
def ui_util_build_help_menu (listener, app_prefix, app_name, parent):
    _bonobo.bonobo_ui_util_build_help_menu (listener._o, app_prefix, app_name, parent._o)
    
def ui_util_build_accel (accelerator_key, accelerator_mods, verb):
    return BonoboUINode (_obj=_bonobo.bonobo_ui_util_build_accel (accelerator_key, accelerator_mods, verb))

def ui_util_new_menu (submenu, name, label, tip, verb):
    return BonoboUINode (_obj=_bonobo.bonobo_ui_util_new_menu (submenu, name, label, tip, verb))

def ui_util_new_placeholder (name, top, bottom):
    return BonoboUINode (_obj=_bonobo.bonobo_ui_util_new_placeholder (name, top, bottom))

def ui_util_set_radiogroup (node, group_name):
    _bonobo.bonobo_ui_util_set_radiogroup (node._o, group_name)

def ui_util_set_toggle (node, id, init_state):
    _bonobo.bonobo_ui_util_set_toggle (node._o, id, init_state)

def ui_util_new_std_toolbar (name, label, tip, verb):
    return BonoboUINode (_obj=_bonobo.bonobo_ui_util_new_std_toolbar (name, label, tip, verb))

def ui_util_new_toggle_toolbar (name, label, tip, id):
    return BonoboUINode (_obj=_bonobo.bonobo_ui_util_new_toggle_toolbar (name, label, tip, id))

def ui_util_get_ui_fname (component_prefix, file_name):
    return _bonobo.bonobo_ui_util_get_ui_fname (component_prefix, file_name)

def ui_util_translate_ui (node):
    _bonobo.bonobo_ui_util_translate_ui (node._o)

def ui_util_fixup_help (component, node, app_prefix, app_name):
    _bonobo.bonobo_ui_util_fixup_help (component._o, node._o, app_prefix, app_name)

def ui_util_fixup_icons (node):
    _bonobo.bonobo_ui_util_fixup_icons (node._o)

def ui_util_new_ui (component, fname, app_prefix, app_name):
    return BonoboUINode (_obj=_bonobo.bonobo_ui_util_new_ui (component._o, fname, app_prefix, app_name))

def ui_util_set_ui (component, app_prefix, file_name, app_name):
    _bonobo.bonobo_ui_util_set_ui (component._o, app_prefix, file_name, app_name)

def ui_util_set_pixbuf (component, path, pixbuf):
    _bonobo.bonobo_ui_util_set_pixbuf (component._o, path, pixbuf)

def ui_util_accel_name (accelerator_key, accelerator_mods):
    return _bonobo.bonobo_ui_util_accel_name (accelerator_key, accelerator_mods)

def ui_util_accel_parse (name, accelerator_key, accelerator_mods):
    _bonobo.bonobo_ui_util_accel_parse (name, accelerator_key, accelerator_mods)

def ui_util_decode_str (str, err):
    return _bonobo.bonobo_ui_util_decode_str (str, err)

def ui_util_encode_str (str):
    return _bonobo.bonobo_ui_util_encode_str (str)
    
# bonobo-widget
class BonoboWidget (gtk.GtkBin):
    get_type = _bonobo.bonobo_widget_get_type
    def __init__ (self, arg="", uic=None, _obj=None):
        if _obj: self._o = _obj; return
        if type(arg) == type(""):
            self._o = _bonobo.bonobo_widget_new_control (arg, uic)
        else:
            self._o = _bonobo.bonobo_widget_new_control_from_objref (arg, uic)

    def get_server (self):
        return _obj2inst (_bonobo.bonobo_widget_get_server (self._o))

    def get_objref (self):
        return _bonobo.bonobo_widget_get_objref (self._o)

    def get_control_frame (self):
        return _obj2inst (_bonobo.bonobo_widget_get_control_frame (self._o))
    
    def get_container (self):
        return _obj2inst (_bonobo.bonobo_widget_get_container (self._o))

    def get_client_site (self):
        return _obj2inst (_bonobo.bonobo_widget_get_client_site (self._o))

    def get_view_frame (self):
        return _obj2inst (_bonobo.bonobo_widget_get_view_frame (self._o))

    def get_uih (self):
        return _obj2inst (_bonobo.bonobo_widget_get_uih (self._o))

    def set_property (self, *args):
        no = len (args)
        if not no or no % 2:
            return
        
        for i in range (0, no, 2):
            if type (args[i]) != type (""):
                return
            _bonobo.bonobo_widget_set_property (self._o, args[i], args[i+1])
    
    def get_property (self, *args):
        retval = []
        for i in range (0, len (args)):
            if type (args[i]) != type (""):
                return
            retval.append (_bonobo.bonobo_widget_get_property (self._o, args[i]))
            
        return tuple (retval)
gtk._name2cls['BonoboWidget'] = BonoboWidget

def widget_new_sub_doc (moniker, uic):
    return _obj2inst (_bonobo.bonobo_widget_new_subdoc (moniker, uic))

# bonobo-view
class BonoboView (BonoboControl):
    get_type = _bonobo.bonobo_view_get_type
    def __init__ (self, widget=None,_obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_view_new (widget)

    def set_embeddable (self, embeddable):
        _bonobo.bonobo_view_set_embeddable (self._o, embeddable._o)
    
    def get_embeddable (self):
        return _obj2inst (_bonobo.bonobo_view_get_embeddable (self._o))

    def set_view_frame (self, view_frame):
        _bonobo.bonobo_view_set_view_frame (self._o, view_frame)

    def get_view_frame (self):
        return _bonobo.bonobo_view_get_view_frame (self._o)

    def get_remote_ui_container (self):
        return _bonobo.bonobo_view_get_remote_ui_container (self._o)

    def get_ui_component (self):
        return _obj2inst (_bonobo.bonobo_view_get_ui_component (self._o))

    def activate_notify (self, activated):
        _bonobo.bonobo_view_activate_notify (self._o, activated)
gtk._name2cls['BonoboView'] = BonoboView

# bonobo-view-frame
class BonoboViewFrame (BonoboControlFrame):
    get_type = _bonobo.bonobo_view_frame_get_type
    def __init__ (self, client_site=None, uih=None,_obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_view_frame_new (client_site, uih)

    def bind_to_view (self, view):
        _bonobo.bonobo_view_frame_bind_to_view (self._o, view)

    def get_view (self):
        return _bonobo.bonobo_view_frame_get_view (self._o)

    def get_client_site (self):
        return _obj2inst (_bonobo.bonobo_view_frame_get_client_site (self._o))

    def get_wrapper (self):
        return _obj2inst (_bonobo.bonobo_view_frame_get_wrapper (self._o))

    def set_covered (self, covered):
        _bonobo.bonobo_view_frame_set_covered (self._o, covered)

    def get_ui_container (self):
        return _bonobo.bonobo_view_frame_get_ui_container (self._o)

    def activate (self):
        _bonobo.bonobo_view_frame_view_activate (self._o)
    
    def deactivate (self):
        _bonobo.bonobo_view_frame_view_deactivate (self._o)

    def set_zoom_factor (self, zoom):
        _bonobo.bonobo_view_frame_set_zoom_factor (self._o, zoom)
gtk._name2cls['BonoboViewFrame'] = BonoboViewFrame

# bonobo-win
class BonoboWindow (gtk.GtkWindow):
    get_type = _bonobo.bonobo_window_get_type
    def __init__ (self, win_name="", title="", _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_window_new (win_name, title)

    def set_contents (self, contents):
        _bonobo.bonobo_window_set_contents (self._o, contents._o)

    def get_contents (self):
        return _obj2inst (_bonobo.bonobo_window_get_contents (self._o))

    def get_ui_engine (self):
        return _obj2inst (_bonobo.bonobo_window_get_ui_engine (self._o))

    def set_name (self, win_name):
         _bonobo.bonobo_window_set_name (self._o, win_name)

    def get_name (self):
        return _bonobo.bonobo_window_get_name (self._o)

    def get_accel_group (self):
        return _obj2inst (_bonobo.bonobo_window_get_accel_group (self._o))

    def freeze (self):
        _bonobo.bonobo_window_freeze (self._o)

    def thaw (self):
        _bonobo.bonobo_window_thaw (self._o)

    def xml_merge (self, path, xml, component):
        return _bonobo.bonobo_window_xml_merge (self._o, path, xml, component)

    def xml_merge_tree (self, path, tree, component):
        return _bonobo.bonobo_window_xml_merge_tree (self._o, path, tree._o, component)

    def xml_get (self, path, node_only):
        return _bonobo.bonobo_window_xml_get (self._o, path, node_only)

    def xml_node_exists (self, path):
        return _bonobo.bonobo_window_xml_node_exists (self._o, path)

    def xml_rm (self, path, by_component):
        return _bonobo.bonobo_window_xml_rm (self._o, path, by_component)
    
    def object_set (self, path, object):
        return _bonobo.bonobo_window_object_set (self._o, path, object)
    
    # def object_get (self, path, object)

    def dump (self, msg):
        _bonobo.bonobo_window_dump (self._o, msg)

    def register_component (self, name, component):
        _bonobo.bonobo_window_register_component (self._o, name, component)

    def deregister_component (self, name):
        _bonobo.bonobo_window_deregister_component (self._o, name)

    def deregister_component_by_ref (self, component):
        _bonobo.bonobo_window_deregister_component_by_ref (self._o, component)
    
    def deregister_dead_components (self):
        _bonobo.bonobo_window_deregister_dead_components (self._o)

    def deregister_get_component_names (self):
        return _bonobo.bonobo_window_deregister_get_component_names (self._o)

    def component_get (self, name):
        return _bonobo.bonobo_window_component_get (self._o, name)

    # FIXME: GtkMenu
    def add_popup (self, popup, path):
        return _bonobo.bonobo_window_add_popup (self._o, popup, path)
    
    def set_ui_container (self, container):
        _bonobo.bonobo_window_set_ui_container (self._o, container._o)

    def remove_popup (self, path):
        _bonobo.bonobo_window_remove_popup (self._o, path)
gtk._name2cls['BonoboWindow'] = BonoboWindow

# bonobo-wrapper
class BonoboWrapper (gtk.GtkBin):
    get_type = _bonobo.bonobo_wrapper_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_wrapper_new ()

    def set_covered (self, covered):
        _bonobo.bonobo_wrapper_set_covered (self._o, covered)

    def is_covered (self):
        return _bonobo.bonobo_wrapper_is_covered (self._o)

    def get_visibility (self):
        return _bonobo.bonobo_wrapper_get_visibility (self._o)

    def set_visibility (self, visible):
        _bonobo.bonobo_wrapper_set_visibility (self._o, visible)
gtk._name2cls['BonoboWrapper'] = BonoboWrapper

# bonobo-zoomable
class BonoboZoomable (BonoboObject):
    get_type = _bonobo.bonobo_zoomable_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_zoomable_new ()

    def set_parameters (self, zoom_level, min_zoom_level, max_zoom_level, has_min_zoom_level, has_max_zoom_level):
        return _bonobo.bonobo_zoomable_set_parameters (self._o, zoom_level, min_zoom_level, max_zoom_level, has_min_zoom_level, has_max_zoom_level)

    # def set_parameters_full (self, zoom_level, min_zoom_level, max_zoom_level, has_min_zoom_level, has_max_zoom_level, is_continuous, preferred_zoom_levels, preferred_zoom_level_names, num_preferred_zoom_levels)
    
    def add_preferred_zoom_level (self, zoom_level, zoom_level_name):
        return _bonobo.bonobo_zoomable_add_preferred_zoom_level (self._o, zoom_level, zoom_level_name)

    def report_zoom_level_changed (self, new_zoom_level):
        return _bonobo.bonobo_zoomable_report_zoom_level_changed (self._o, new_zoom_level)
    
    def report_zoom_parameters_changed (self):
        return _bonobo.bonobo_zoomable_report_zoom_parameters_changed (self._o)
gtk._name2cls['BonoboZoomable'] = BonoboZoomable

def zoomable_corba_object_create (object):
    return _bonobo.bonobo_zoomable_corba_object_create (object._o)

# bonobo-zoomable-frame
class BonoboZoomableFrame (BonoboObject):
    get_type = _bonobo.bonobo_zoomable_frame_get_type
    def __init__ (self, _obj=None):
        if _obj: self._o = _obj; return
        self._o = _bonobo.bonobo_zoomable_frame_new ()

    def get_zoom_level (self):
        return _bonobo.bonobo_zoomable_frame_get_zoom_level (self._o)

    def get_min_zoom_level (self):
        return _bonobo.bonobo_zoomable_frame_get_min_zoom_level (self._o)

    def get_max_zoom_level (self):
        return _bonobo.bonobo_zoomable_frame_get_max_zoom_level (self._o)

    def has_min_zoom_level (self):
        return _bonobo.bonobo_zoomable_frame_has_min_zoom_level (self._o)

    def has_max_zoom_level (self):
        return _bonobo.bonobo_zoomable_frame_has_max_zoom_level (self._o)

    def is_continuous (self):
        return _bonobo.bonobo_zoomable_frame_is_continuous (self._o)

    # def get_preferred_zoom_levels (self)
    # def get_preferred_zoom_level_names (self)
    
    def set_zoom_level (self):
        _bonobo.bonobo_zoomable_frame_set_zoom_level (self._o)

    def zoom_in (self):
        _bonobo.bonobo_zoomable_frame_zoom_in (self._o)

    def zoom_out (self):
        _bonobo.bonobo_zoomable_frame_zoom_out (self._o)

    def zoom_to_fit (self):
        _bonobo.bonobo_zoomable_frame_zoom_to_fit (self._o)

    def zoom_to_default (self):
        _bonobo.bonobo_zoomable_frame_zoom_to_default (self._o)

    def bind_to_zoomable (self, zoomable):
        _bonobo.bonobo_zoomable_frame_bind_to_zoomable (self._o, zoomable)

    def get_zoomable (self):
        return _bonobo.bonobo_zoomable_frame_get_zoomable (self._o)
gtk._name2cls['BonoboZoomableFrame'] = BonoboZoomableFrame

def zoomable_frame_corba_object_create (object):
    return _bonobo.bonobo_zoomable_frame_corba_object_create (object._o)

#
# private functions
#
class conv:
    def __init__ (self, func):
        self.func = func
    def __call__ (self, *args):
        a = list (args)
        for i in range (len (args)):
            if type (args[i]) == _gtk.GtkObjectType:
                a[i] = _obj2inst (args[i])
            elif type (args[i]) == _gtk.GtkAccelGroupType:
                a[i] = GtkAccelGroup (_obj=args[i])
        a = tuple (a)
        ret = apply (self.func, a)
        if hasattr (ret, '_o'):
            ret = ret._o
        return ret

