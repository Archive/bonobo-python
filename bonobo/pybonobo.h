/* bonobo-python - Bonobo bindings for python.
 * Copyright (C) 2001 Johan Dahlin <zilch.am@home.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __PYBONOBO_H__
#define __PYBONOBO_H__

#include <Python.h>
#include <pygtk/pygtk.h>
#include <pygtk/pygdkpixbuf.h>
#include <orbit-python/orbit-python.h>
#include <liboaf/liboaf.h>
#include <bonobo/bonobo-property-bag-client.h>
#include <bonobo/bonobo-desktop-window.h>
#include <bonobo/bonobo-ui-toolbar-item.h>
#include <bonobo/bonobo-moniker-extender.h>
#include <bonobo/bonobo-ui-toolbar-toggle-button-item.h>
#include <bonobo/bonobo-ui-toolbar-icon.h>
#include <bonobo/bonobo-object-directory.h>
#include <bonobo/bonobo-socket.h>
#include <bonobo/bonobo-item-handler.h>
#include <bonobo/bonobo-stream-memory.h>
#include <bonobo/bonobo-persist-file.h>
#include <bonobo/bonobo-canvas-item.h>
#include <bonobo/bonobo-storage.h>
#include <bonobo/bonobo-storage-plugin.h>
#include <bonobo/bonobo-control-frame.h>
#include <bonobo/bonobo-plug.h>
#include <bonobo.h>

typedef struct {
    PyObject_HEAD
    BonoboUINode *obj;
} PyBonoboUINode;

#define PyBonoboUINode_Get(v)         (((PyBonoboUINode *)(v))->obj)

staticforward PyTypeObject PyBonoboUINode_Type;

static PyObject *PyBonoboUINode_New (BonoboUINode *node);

/* FIXME: Remove when next version of gnome-python is released */
#define my_init_pygdkpixbuf() { \
	PyObject *pygtk = PyImport_ImportModule("gdkpixbuf"); \
	if (pygtk != NULL) { \
		PyObject *module_dict = PyModule_GetDict(pygtk); \
		PyObject *cobject = PyDict_GetItemString(module_dict, "_PyGdkPixbuf_API"); \
		if (PyCObject_Check(cobject)) \
			_PyGdkPixbuf_API = PyCObject_AsVoidPtr(cobject); \
		else { \
			Py_FatalError("could not find _PyGdkPixbuf_API object"); \
			return; \
		} \
	} else { \
		Py_FatalError("could not import gdkpixbuf"); \
		return; \
	} \
}

#endif /* __PYBONOBO_H__ */
