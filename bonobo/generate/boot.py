#!/usr/bin/env python
import os
import string
import time

PYTHON = "python"

fd = os.popen ("gnome-config bonobo --includedir 2> /dev/null")
includedir = fd.readline()[:-1]
if not includedir:
    print "ERROR: Cannot find bonobo includes."
    raise SystemExit

files = []
for file in os.listdir (includedir + "/bonobo"):
    if file == "Bonobo.h":
	continue
    else:
	files.append (includedir + "/bonobo/" + file)

os.system ("rm -f bonobo.defs")	
for file in files:
    output = string.split (file, "/")[-1]
    
    cmd = str(PYTHON + " h2def.py " + file + " >> bonobo.defs")

    os.system (cmd)

