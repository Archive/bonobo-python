#!/usr/bin/env python
import defsparser
import getopt
import string
import sys

from dicts import *
from templates import *

# Convert an of_object to a params like type
def of2param (of):
    both = of[1] + of[0]

    if both == 'BonoboArgType' or both[:7] == 'Bonobo_':
        return both, of[0]
    else:
        return '%s*' % (both), of[0]

class CCreator:
    def __init__ (self, func, name):
        self.func = func
        self.name = name

        self._variables = []           # All variables
        self.params = []               # All params to func
        self.tuple_args = []           # All params to PyArg_ParseTuple
        self.tuple_format_args = ''    # PyArg_ParseTuple format string
        self.args = []
        self.ev = ''
        self.extra = ''
	self.after = ''
	self.before = ''
	
        try:
            self.params.append (of2param (self.func.of_object))
            self.params = self.params + self.func.params
        except:
            try:
                self.params = func.params
            except:
                self.params = None

    def have_params (self):
        return self.params
    
    def funcname2arg (self, name):
        if format_dict.has_key (name):
            return format_dict[name]
#       if format2_dict.has_key (name):
#            return format2_dict[name]        
        elif type_dict.has_key (name):
            return 'O!'
        else:
            return 'O'

    def convertArg (self, arg):
        if convert_dict.has_key (arg):
            return convert_dict[arg]
        else:
            return arg
                
    def doParams (self):
        for arg in self.params:
            var = self.convertArg (arg[0])
                
            # Do this after the first conversation
            a = self.funcname2arg (var)
            self.tuple_format_args = self.tuple_format_args + a

            # Then, create arguments for PyArg_ParseTuple
            if a == 'O!':
                if type_dict.has_key (var):
                    self.tuple_args.append (type_dict[var]['type'])
                    var = type_dict[var]['obj']
                    
                self.tuple_args.append (arg[1])
                if var == 'CORBA_any*':
                    self.addVar ('PyObject*', arg[1])                    
                else:
                    self.addVar (var, arg[1])
            elif a:
                if self.isCorbaObject (var) or var == 'GList*':
		    new = self.addVar ('PyObject*', arg[1])
                else:
		    new = self.addVar (var, arg[1])

                self.tuple_args.append (new)
            else:
                name = arg[1]
		# CORBA_Environment is a special case.
                if name in ['ev', 'opt_ev']:
                    name = self.addCorbaEnv (name)
                    self.args.append ('&%s' % name)
                else:
                    self.args.append (name)
                    self.addVar (var, name)
                continue

            # Then, create the arguments to the function
            var = self.convertArg (arg[0])
                
            # Cast and _Get
            if cast_dict.has_key (var):
                if type_dict.has_key (var):
                    object = '%s (%s)' % (type_dict[var]['get'], arg[1])
                else:
                    object = arg[1]
		
                self.args.append ('%s (%s)' % (cast_dict[var], object))
            # Only _Get
            elif type_dict.has_key (var) and arg[0] != 'CORBA_Object':
                self.args.append ('%s (%s)' % (type_dict[var]['get'], arg[1]))
            # else, CORBA_Object, char* etc.
            else:
                if self.isCorbaObject (arg[0]):                    
                    self.addCorbaEnv ()
                    
                    # Duplicate an object (CORBA way)
                    self.args.append ('CORBA_Object_duplicate (((CORBA_Object)PyORBit_Object_Get (%s)), &ev)' % (arg[1]))
                else:
                    self.args.append (arg[1])

    def isCorbaObject (self, obj):
        if obj == 'CORBA_Object':
            return 1
#        elif obj[:7] == 'Bonobo_' and obj[-2:] != 'Id':
        elif string.find (obj, 'Bonobo_') != -1 and obj[-2:] != 'Id':            
            return 1
        return 0
    
    def addCorbaEnv (self, name='ev'):
        if self.ev:
            return self.ev

        self.ev = name
        self.addVar ('CORBA_Environment', name)
        
        self.addBefore ('CORBA_exception_init (&%s)' % name)
        self.addAfter  ('CORBA_exception_free (&%s)' % name)
        
        return name
            
    def doRetval (self):
        type = self.func.ret
        if type in ['none', 'void']:
            self.ret = return_none_template
            self.retval = ''
        elif type in format_dict.keys():
            if type in GLIST_TYPES:
                self.makeRetval (template=return_list_template)
            elif type in ORB_TYPES:
                self.makeRetval (template=return_orb_template)
            elif type in TYPECODE_TYPES:
                self.makeRetval (template=return_tc_template)
            else:
                self.makeRetval (format_dict[type])
        elif self.isCorbaObject (type):
            self.addCorbaEnv ()
            self.makeRetval (template=return_corba_template)
            self.retval = self.retval + 'CORBA_Object_duplicate ('
            self.extra = '%s, &%s' % (self.extra, self.ev)
        elif type == 'GtkType':
            self.makeRetval ('i', name='type')
        else:
            self.makeRetval (template=return_gtkobj_template)
            
            if cast_dict.has_key (self.func.ret):
                self.retval = self.retval + '%s (' % (cast_dict[self.func.ret])
                    
    def makeRetval (self, type=None, template=None, name='retval'):
	if type:
	    self.ret = return_buildvalue_template % (type, name)
	else:
	    self.ret = template % name
	    
	self.retval = '%s = ' % name
	self.addVar (self.func.ret, name)
	
    def addVar (self, type, name):
        if name == 'args':
            name = 'funcargs'
        self._variables.append ('%s %s;' % (type, name))
        return name
    
    def doComment (self):
        p = ''
        for arg in self.params:
            if convert_dict.has_key (arg[0]):
		p = p + '%s %s, ' % (convert_dict[arg[0]], arg[1])		
            else:
		p = p + '%s, ' % (string.join (arg[:2]))
        p = p[:-2]
	
        if self.func.ret == 'none':
	    self.comment = 'void %s (%s)' % (self.name, p)	    
        else:
	    self.comment = '%s %s (%s)' % (self.func.ret, self.name, p)
	    
    def addBefore (self, command):
	fmt = '\t%s;\n' % command
	self.before = self.before + fmt
	
    def addAfter (self, command):
	fmt = '\t%s;\n' % command
	self.after = self.after + fmt	
	
    def fixVariables (self):
        self._variables = string.join (self._variables, '\n\t')
        self._variables = self._variables + '\n'        

    def fixTupleArgs (self):
        if len (self.tuple_args):
            # "format", &first_var, &second_var, ... )
            extra = ', &' + string.join (self.tuple_args, ', &')
        else:
            extra = ''
            
        self.tuple_args = '"%s:%s"%s' % (self.tuple_format_args, self.name, extra)
            
    def fixArgs (self):
        if len (self.args):
            self.args = string.join (self.args, ', ')
        else:
            self.args = ''

        for no in range (string.count (self.retval, '(')):
            self.args = self.args + ')'
	
	self.args = self.args + self.extra

    def doRest (self):
	self.doComment()
        self.doRetval()

        self.fixVariables ()
        self.fixTupleArgs ()
        self.fixArgs ()
        
    def __repr__ (self):
        return standard_template % { 'name'       : self.name, 
                                     'variables'  : self._variables, 
                                     'retval'     : self.retval, 
                                     'ret'        : self.ret,
                                     'args'       : self.args, 
                                     'after'      : self.after,
                                     'before'     : self.before, 
                                     'tuple_args' : self.tuple_args, 
                                     'comment'    : self.comment }


# This is a quick hack.				
class PythonCreator:
    def __init__ (self, dp, ignore, fname):
        self.constructor = []
        self.constructor_params = ''
        self.get_type_func = ''
        self.funcs = []
        self.class_name = 'Unknown'
        self.fname = fname
        for funcname in dp.c_name.keys():
            if funcname in ignore:
                continue
            func = dp.c_name[funcname]
            self.doFunc (func)
            
    def doFunc(self, func):
        try:
            func.ret
        except:
            return
        
        if len (func.params):
            list = func.params
            params = ''
            for i in range (len (list)):
                if string.find (list[i][1], 'ev') != -1:
                    continue
                if list[0][-1] == '*' and list[0][0] == 'B':
                    continue
                params = params + list[i][1] + ', '
            params = params [:-2]
        else:
            params = ''
        
        # Constructors
        if not self.isObject (func):
            if func.ret == 'GtkType':
                self.get_type_func = func.c_name
            else:
                self.constructor.append ([func.c_name, params])
                               
                self.class_name = func.ret[:-1]
        else:
            self.funcs.append ([func.c_name, params])
              
    def isObject (self, func):
        try:
            return func.of_object
        except:
            return None

    def __repr__ (self):
        defs = \
        "# %(filename)s\n" + \
        "class %(class_name)s:\n" + \
        "   get_type = _bonobo.%(get_type_func)s()\n" + \
        "   def __init__ (self, %(constructor_params)s_obj=None):\n" + \
        "       if _obj: self._o = _obj; return\n" + \
        "%(constructor)s\n\n" + \
        "%(functions)s"
        funcs = ""
        for name, params in self.funcs:
            if not len(params):
                p = ""
            else:
                p = ", " + params
                
            funcs = funcs + "    def %s (self%s):\n        return _bonobo.%s (self._o%s)\n\n" % (name, p, name, p)
            
        new_cons = ""
        for cons in self.constructor:
            new_cons = new_cons + "        self._o = _bonobo.%s (%s)\n" % (cons[0], cons[1])
            
        return defs % {"filename"           : self.fname,
                       "class_name"         : self.class_name,
                       "get_type_func"      : self.get_type_func,
                       "constructor"        : new_cons,
                       "constructor_params" : self.constructor_params,
                       "functions"          : funcs}

def ignoredFunctions ():
    list = []
    fd = open ("bonobomodule.ignore")
    for line in fd.readlines():
        if line[0] == "#":
            continue
        list.append (line[:-1])
    return list

options, arguments = getopt.getopt(sys.argv[1:], "", ["impl=", "defs=",
                                                      "py=", "source="])

impl = defs = py = source = 0
for (opt, optarg) in options:
    if opt == "--source":
        source = optarg
        source_file = open (optarg, "r")
        source_filename = string.split (string.split (optarg, "/")[-1], ".")[0]
    if opt == "--impl":
        impl = optarg
        impl_file = open (optarg, "a")
    elif opt == "--defs":
        defs = optarg
        defs_file = open (optarg, "a")
    elif opt == "--py":
        py = optarg
        py_file = open (optarg, "a")
        
if not source:
    print "Must enter source."
    raise SystemExit
    
# Main
dp = defsparser.DefsParser (source_file)
dp.startParsing()

ignore = ignoredFunctions()

if py:
    fp = PythonCreator (dp, ignore, source_filename)
    py_file.write (str (fp))
    
for funcname in dp.c_name.keys():
    if funcname in ignore:
        continue
    
    func = dp.c_name[funcname]
    if defs :
        defs_file.write('{ "%s", %50s, METH_VARARGS },\n' % (funcname,
                                                 ("_wrap_" + funcname)))

    if impl:
        inst = CCreator (func, funcname)
        if inst.have_params():
            inst.doParams()
        inst.doRest()
        
        impl_file.write (str(inst) + "\n")

source_file.close()

if defs:
    defs_file.close()
    
if impl:
    impl_file.close()

if py:
    py_file.close()
