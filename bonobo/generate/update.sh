#!/bin/sh
IMPL="../bonobomodule_impl.c"
DEFS="../bonobomodule_defs.c"
SOURCE="bonobo.defs"
PYTHON="python"

rm -f $IMPL $DEFS
$PYTHON generate.py --impl $IMPL --defs $DEFS  --source "bonobo.defs"

