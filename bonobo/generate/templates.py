header_template =              \
'#include <Python.h>\n'      + \
'#include <pygtk/pygtk.h>\n' + \
'#include <pybonobo.h>\n'

standard_template =                                                    \
'/* %(comment)s\n'                                                   + \
' *\n'                                                               + \
' */\n'                                                              + \
'static PyObject* _wrap_%(name)s (PyObject* self, PyObject* args)\n' + \
'{\n'                                                                + \
'\t%(variables)s\n'                                                  + \
'\tif (!PyArg_ParseTuple (args, %(tuple_args)s))\n'                  + \
'\t\treturn NULL;\n'                                                 + \
'\n'                                                                 + \
'%(before)s'                                                         + \
'\t%(retval)s%(name)s (%(args)s);\n'                                 + \
'%(after)s'                                                          + \
'\n'                                                                 + \
'\t%(ret)s;\n'                                                       + \
'}\n'

return_none_template = \
'Py_INCREF (Py_None);\n' + \
'\treturn Py_None'

return_buildvalue_template = 'return Py_BuildValue ("%s", %s)'
return_list_template       = 'return GList_ToPyList (%s)'
return_gtkobj_template     = 'return PyGtk_New (GTK_OBJECT (%s))'
return_orb_template        = 'return (PyObject*)PyORBit_ORB_New (%s)'
return_tc_template         = 'return (PyObject *)PyORBit_TypeCode_New (%s)'
return_corba_template      = 'return (PyObject *)PyORBit_Object_New (%s)'
return_node_template       = 'return PyBonoboUINode_New (%s))'
