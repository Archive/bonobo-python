import bonobo
import gtk
from gtk import TRUE, FALSE

def test_gtkhtml ():
    window = bonobo.BonoboWindow('test-editor', 'GtkHTML editor')
    window.connect ('delete_event', gtk.mainquit)
    window.set_default_size (500, 440)
    window.set_policy (TRUE, TRUE, FALSE)

    container = bonobo.BonoboUIContainer ()
    container.set_win (window)

    control = bonobo.BonoboWidget ("OAFIID:GNOME_GtkHTML_Editor", container.corba_objref ())
    if not control:
	print 'Cannot get OAFIID:GNOME_GtkHTML_Editor'
	gtkmainquit ()

    window.set_contents (control)
    window.show_all ()
    
    return FALSE

gtk.idle_add (test_gtkhtml)
bonobo.main ()
				    
