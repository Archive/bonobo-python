#!/usr/bin/env python
import urllib
import nautilus
import gtk
import gtkhtml

class HTMLWidget (gtk.GtkScrolledWindow):
    def __init__ (self):
	gtk.GtkScrolledWindow.__init__ (self)
	self.set_policy (gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

	self.html = gtkhtml.GtkHTML ()
	self.html.connect ('url_requested', self.request_url)
	self.html.load_empty ()
	self.add (self.html)

    def load_data (self, data):
	handle = self.html.begin ()
	self.html.write (handle, data)
	self.html.end (handle, gtkhtml.HTML_STREAM_OK)
	
    def request_url (self, html, url, handle):
        data = urllib.urlopen (url).read ()
	html.write (handle, data)

class Component (nautilus.NautilusView):
    def __init__ (self):
	self.widget = HTMLWidget ()
        self.widget.show_all ()
        
	nautilus.NautilusView.__init__ (self, self.widget)
        self.connect ("load_location", self.load_location)

    def load_location (self, object, filename):

        self.report_load_underway ()
        data = urllib.urlopen (filename).read ()
        self.widget.load_data (data)
        self.report_load_complete ()

nautilus.view_standard_main ("OAFIID:nautilus_gtkhtmlpy_content_view_factory:020a0285-6b96-4685-84a1-4a56eb6baa2b",
                             Component)
