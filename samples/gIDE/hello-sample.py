#!/usr/bin/env python
import bonobo
import BONOBO

# IDL
from GNOME.Development import Environment

FACTORY_IID = "OAFIID:GNOME_Development_Plugin:python-factory"

# UI data
ui_data = """<Root>
<commands>
  <cmd name="HelloWorldPy" _label="Hello, World (python)"/>
</commands>
<menu>
  <submenu name="Tools">
    <menuitem name="HelloWorldPy" verb=""/>
  </submenu>
</menu>
</Root>"""

def hello_world (*args):
    import gnome.ui
    dialog = gnome.ui.GnomeOkDialog ("Hello World!")
    dialog.show ()

def get_prop (*args): pass

def set_prop (pb, arg, idx):
    shell = bonobo.get_object ("gide:%s" % arg.value,
                               "IDL:GNOME/Development/Environment/Shell:1.0")
    uic = shell.getUIContainer ()
    
    # Register menu item
    comp = bonobo.BonoboUIComponent ("gide-sample-python-plugin")
    comp.set_container (uic)
    comp.set_translate ("/", ui_data)
    comp.add_verb ("HelloWorldPy", hello_world)
    
    shell.unref ()
    
# Factory Callback
def factory_fn (object, object_id):
    pb = bonobo.BonoboPropertyBag (get_prop, set_prop)
    
    pb.add ('gide-shell', 1,
            BONOBO.ARG_STRING, 
	    None,
	    'The id of the shell that owns the plugin.',
	    BONOBO.PROPERTY_WRITEABLE)
	    
    return pb

bonobo.BonoboGenericFactory (FACTORY_IID,
                             factory_fn)
bonobo.main ()
