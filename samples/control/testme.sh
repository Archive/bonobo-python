#!/bin/sh
OAF_DEBUG_OUTPUT=1
PATH=$PATH:.
OAF_INFO_PATH=$OAF_INFO_PATH:`pwd`

echo "Removing stale processes"
kill -9 `pidof oafd` > /dev/null

echo "Starting the component..."
./control.py &

echo "Launching gshell..."
gshell
