#!/usr/bin/env python
# control.py - Sample bonobo control in python
import bonobo
import gtk

# This function is used to create our factory
# It's called inside bonobo.main ()
def control_factory (factory, object_id):

    # Create a GtkWidget and show it
    label = gtk.GtkLabel ("Sample Control")
    label.show()
    
    # Create a control from the widget
    control = bonobo.BonoboControl (label)
    
    # Finally, return the control, so the factory
    # know what it should create
    return control

# Create a Factory, default is to create a multi factory.
bonobo.BonoboGenericFactory ("OAFIID:GNOME_SampleControlFactory",
                             control_factory)

# Bonobo main, everything happends here
bonobo.main ()

